package com.br.store.crud;

import java.util.List;

import com.br.store.exception.StoreException;

public interface AbstractCrud<T> {
	
	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	public T findOne(Long id) throws StoreException;
	
	public List<T> findAll() throws StoreException;
	
	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	public T save(T entity) throws StoreException;
	
	public List<T> saveAll(List<T> entities) throws StoreException;
	
	public T update(T entity, Long id) throws StoreException;
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	public void delete(Long id) throws StoreException;
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	public void validate(T entity) throws StoreException;

}
