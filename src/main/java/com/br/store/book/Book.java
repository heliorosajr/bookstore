package com.br.store.book;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "book")
@Data
@ToString
public class Book {
	
	/**
	 * Book identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Book title.
	 */
	@Column(name = "title", nullable = false, length = 500)
	private String title;

	/**
	 * Book description.
	 * <br/>
	 * Required with max length 500.
	 */
	@Column(name = "description", nullable = false, length = 500)
	private String description;
	
	/**
	 * Book author.
	 */
	@Column(name = "authors", nullable = false, length = 500)
	private String authors;
	
	/**
	 * Book pages.
	 */
	@Column(name = "page_count")
	private Integer pageCount;
	
	/**
	 * Book category.
	 */
	@Column(name = "categories", nullable = true, length = 500)
	private String categories;
	
	/**
	 * Book ISBN 13.
	 */
	@Column(name = "isbn_13", unique = true, length = 13)
	private String isbn13;
	
	/**
	 * Book ISBN 10.
	 */
	@Column(name = "isbn_10", unique = true, length = 10)
	private String isbn10;
	
	/**
	 * Book quantity.
	 */
	@Column(name = "quantity", nullable = false)
	private int quantity;
	
	/**
	 * Book price.
	 */
	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
}