package com.br.store.book.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.store.book.Book;
import com.br.store.book.service.BookService;
import com.br.store.error.CustomError;
import com.br.store.exception.StoreException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Book", tags = { "Book" })
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	private BookService bookService;

	@Autowired
	private BookModelAssembler assembler;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Book list retrieved", response = Book.class, responseContainer = "List"),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "List all books", response = Book.class, responseContainer = "List")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find-all", method = RequestMethod.GET)
	public CollectionModel<EntityModel<Book>> findAll() throws StoreException {

		List<Book> books = bookService.findAll();

		return assembler.toCollectionModel(books);
	}

	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Book retrieved", response = Book.class),
		    @ApiResponse(code = 404, message = "Book not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Find book by id", response = Book.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public EntityModel<Book> findOne(
		@ApiParam(name = "id", value = "Book id") @PathVariable Long id) throws StoreException {

		Book book = bookService.findOne(id);
		
		return assembler.toModel(book);
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "Book created", response = Book.class),
		    @ApiResponse(code = 400, message = "Bad request", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Create book", response = Book.class)
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(
		@ApiParam(name = "book", value = "Book data in JSON format") @RequestBody Book book) throws StoreException {

		EntityModel<Book> entityModel = assembler.toModel(bookService.save(book));

		return ResponseEntity
				.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(entityModel);
	}

	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Book deleted"),
		    @ApiResponse(code = 404, message = "Book not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Delete book")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(
		@ApiParam(name = "id", value = "Book id") @PathVariable Long id) throws StoreException {
		bookService.delete(id);
		
		return ResponseEntity.noContent().build();
	}
	
	// ----------------------------------------------------
	// Update
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Book updated", response = Book.class),
		    @ApiResponse(code = 404, message = "Book not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Update Book", response = Book.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public EntityModel<Book> update(
		@ApiParam(name = "book", value = "Book data in JSON format") @RequestBody Book book,
		@ApiParam(name = "id", value = "Book id") @PathVariable Long id) throws StoreException {
		book = bookService.update(book, id);
		
		return assembler.toModel(book);
	}
	
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Quantity updated", response = Book.class),
		    @ApiResponse(code = 404, message = "Book not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Increase quantity of book in stock", response = Book.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/increase-stock/{id}", method = RequestMethod.PUT)
	public EntityModel<Book> increaseStock(
		@ApiParam(name = "book", value = "Book data in JSON format") @RequestBody Book book,
		@ApiParam(name = "id", value = "Book id") @PathVariable Long id) throws StoreException {
		book = bookService.increaseStock(book, id);
		
		return assembler.toModel(book);
	}
	
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Quantity updated", response = Book.class),
		    @ApiResponse(code = 404, message = "Book not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Decrease quantity of book in stock", response = Book.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/remove-from-stock/{id}", method = RequestMethod.PUT)
	public EntityModel<Book> removeFromStock(
		@ApiParam(name = "book", value = "Book data in JSON format") @RequestBody Book book,
		@ApiParam(name = "id", value = "Book id") @PathVariable Long id) throws StoreException {
		book = bookService.removeFromStock(book, id);
		
		return assembler.toModel(book);
	}

}
