package com.br.store.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.book.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}