package com.br.store.book.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.book.Book;
import com.br.store.book.repository.BookRepository;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository repository;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public Book findOne(Long id) throws StoreException {
		try {
			Optional<Book> book = repository.findById(id);
			
			if(book.isPresent()) {
				return book.get();
			}
			
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.ENTITY_NOT_FOUND, "Entity not found", "Could not retrieve book with id " + id, HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Error retrieving book with id " + id, e);
		}
	}
	
	@Override
	public List<Book> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not retrieve books" + e.getMessage(), e);
		}
	}
	
	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public Book save(Book book) throws StoreException {
		try {
			return repository.save(book);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not save book", e);
		}
	}
	
	@Override
	public List<Book> saveAll(List<Book> books) throws StoreException {
		try {
			return repository.saveAll(books);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not save books", e);
		}
	}
	
	@Override
	public Book update(Book book, Long id) throws StoreException {
		try {
			findOne(id);
			return save(book);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not update book with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			Book book = findOne(id);
			repository.delete(book);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not delete book with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Update stock
	// ----------------------------------------------------
	@Override
	public Book increaseStock(Book book, Long id) throws StoreException {
		return modifyStock(book, id, true);
	}

	@Override
	public final Book removeFromStock(Book book, Long id) throws StoreException {
		return modifyStock(book, id, false);
	}
	
	private Book modifyStock(Book book, Long id, boolean adding) throws StoreException {
		Book bookFromDb = null;
		
		try {
			bookFromDb = findOne(id);
			if(adding) {
				bookFromDb.setQuantity(bookFromDb.getQuantity() + book.getQuantity());
			} else {
				bookFromDb.setQuantity(bookFromDb.getQuantity() - book.getQuantity());
			}
		
			return repository.save(bookFromDb);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not update stock for book " + book.getId(), e);
		}
	}
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(Book book) throws StoreException {

	}

}