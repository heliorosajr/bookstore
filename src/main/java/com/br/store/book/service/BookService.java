package com.br.store.book.service;

import com.br.store.book.Book;
import com.br.store.crud.AbstractCrud;
import com.br.store.exception.StoreException;

public interface BookService extends AbstractCrud<Book> {
	
	// ----------------------------------------------------
	// Update stock
	// ----------------------------------------------------
	public Book increaseStock(Book book, Long id) throws StoreException;
	
	public Book removeFromStock(Book book, Long id) throws StoreException;

}
