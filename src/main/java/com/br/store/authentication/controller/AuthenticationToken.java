package com.br.store.authentication.controller;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class AuthenticationToken {
	
	private String user;
	
	private String token;
	
	private LocalDateTime generatedAt;
	
	private int expiresIn;

}
