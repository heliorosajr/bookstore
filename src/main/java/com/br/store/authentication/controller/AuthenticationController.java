package com.br.store.authentication.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.store.error.CustomError;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.security.TokenProvider;
import com.br.store.user.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/auth")
@Api(value = "Authentication", tags = { "Authentication" })
public class AuthenticationController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenProvider tokenProvider;
	
	@Autowired
	private UserService userService;
	
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "Authentication succeeded", response = AuthenticationToken.class),
		    @ApiResponse(code = 401, message = "Authentication failed", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Authenticates user", response = AuthenticationToken.class)
	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping("/signin")
	public ResponseEntity<AuthenticationToken> signin(
		@ApiParam(name = "authenticationData", value = "Authentication data in JSON format") @RequestBody AuthenticationData authenticationData) throws StoreException {
		try {
			String username = authenticationData.getUsername();
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, authenticationData.getPassword()));
			
			String token = tokenProvider.createToken(username, userService.findByUsername(username).getRoles());
			
			AuthenticationToken authToken = new AuthenticationToken();
			authToken.setUser(authenticationData.getUsername());
			authToken.setToken(token);
			authToken.setGeneratedAt(LocalDateTime.now());
			authToken.setExpiresIn(3600);
			
			 return new ResponseEntity<AuthenticationToken>(authToken, HttpStatus.CREATED);
		} catch (AuthenticationException e) {
			throw new StoreException(ErrorDomain.AUTHENTICATION, ErrorCode.AUTHENTICATION_FAILED, "Invalid username/password supplied", e, HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.AUTHENTICATION, ErrorCode.SERVER_ERROR, "Could not authenticate user", e);
		}
	}
}