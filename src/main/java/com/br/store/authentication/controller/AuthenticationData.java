package com.br.store.authentication.controller;

import lombok.Data;

@Data
public class AuthenticationData {
	
	private String username;
	
    private String password;

}
