package com.br.store.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.address.Address;
import com.br.store.city.City;
import com.br.store.city.service.CityService;
import com.br.store.email.address.EmailAddress;
import com.br.store.employee.Employee;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.phone.Phone;
import com.br.store.user.User;
import com.br.store.user.repository.UserRepository;
import com.br.store.utils.Constants;
import com.br.store.utils.ValidationUtils;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private CityService cityService;

	@Override
	public User findByUsername(String username) throws StoreException {
		try {
			User user = repository.findByUsername(username);
			
			if(user != null) {
				return user;
			}
			
			throw new StoreException(ErrorDomain.USER, ErrorCode.ENTITY_NOT_FOUND, Constants.ENTITY_NOT_FOUND,
				String.format(Constants.ENTITY_NOT_FOUND_GENERIC_MESSAGE, "user", "username", username), HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.USER, ErrorCode.SERVER_ERROR,
				"Error retrieving user with username " + username, e);
		}
	}

	@Override
	public void validate(User user) throws StoreException {
		ValidationUtils.checkIfEmpty(user.getFirstName(), ErrorDomain.USER, "User first name");
		ValidationUtils.checkIfExceeds(user.getFirstName(), 255, ErrorDomain.USER, "User first name");
		ValidationUtils.checkIfExceeds(user.getLastName(), 255, ErrorDomain.USER, "User last name");
		
		if(user instanceof Employee) {
			ValidationUtils.checkIfEmpty(user.getDateOfBirth(), ErrorDomain.EMPLOYEE, "Employee date of birth");
		}
		ValidationUtils.checkIfDateIsInFuture(user.getDateOfBirth(), ErrorDomain.USER, "User date of birth");

		validateAddress(user);
		validateEmails(user);
		validatePhoneNumbers(user);
		validateUsernameAndPassword(user);
	}
	
	private void validateAddress(User user) throws StoreException {
		Address address = user.getAddress();
		
		if(user instanceof Employee) {
			ValidationUtils.checkIfEmpty(address, ErrorDomain.EMPLOYEE, "Employee address");
		}
		
		// address information
		ValidationUtils.checkIfEmpty(address.getAddressInformation(), ErrorDomain.USER, "Address information");
		ValidationUtils.checkIfExceeds(address.getAddressInformation(), 500, ErrorDomain.USER, "Address information");
		
		// CEP
		ValidationUtils.checkIfEmpty(address.getCep(), ErrorDomain.USER, "Address CEP");
		ValidationUtils.checkIfExceeds(address.getCep(), 8, ErrorDomain.USER, "Address CEP");
		
		// if City does not exist, it will be created
		if(address.getCity() != null) {
			City city = cityService.save(address.getCity());
			address.setCity(city);
		}
		
		// city
		ValidationUtils.checkIfEmpty(address.getCity(), ErrorDomain.USER, "Address city");
	}
	
	private void validateEmails(User user) throws StoreException {
		for(EmailAddress email : user.getEmails()) {
			ValidationUtils.checkIfExceeds(email.getEmail(), 255, ErrorDomain.USER, "User email address");
			ValidationUtils.isEmailValid(email.getEmail(), ErrorDomain.USER, "User email address");
			ValidationUtils.checkIfExceeds(email.getDescription(), 100, ErrorDomain.USER, "User email description");
		}
	}

	private void validatePhoneNumbers(User user) throws StoreException {
		for(Phone phone : user.getPhoneNumbers()) {
			ValidationUtils.checkIfExceeds(phone.getNumber(), 15, ErrorDomain.USER, "User phone number");
			ValidationUtils.checkIfExceeds(phone.getDescription(), 100, ErrorDomain.USER, "User phone number description");
		}
	}
	
	private void validateUsernameAndPassword(User user) throws StoreException {
		ValidationUtils.checkIfEmpty(user.getUsername(), ErrorDomain.USER, "Username");
		ValidationUtils.checkIfExceeds(user.getUsername(), 60, ErrorDomain.USER, "Username");

		User userFromDb = repository.findByUsername(user.getUsername());
		if(userFromDb != null && !userFromDb.getId().equals(user.getId())) {
			throw new StoreException(ErrorDomain.USER, ErrorCode.BAD_REQUEST, Constants.INVALID_VALUES,
				"Username already in use", HttpStatus.BAD_REQUEST);
		}

		ValidationUtils.checkIfEmpty(user.getPwd(), ErrorDomain.USER, "User password");
		ValidationUtils.checkIfExceeds(user.getPwd(), 60, ErrorDomain.USER, "User password");
	}

}
