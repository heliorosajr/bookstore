package com.br.store.user.service;

import com.br.store.exception.StoreException;
import com.br.store.user.User;

public interface UserService {
	
	public User findByUsername(String username) throws StoreException;
	
	public void validate(User user) throws StoreException;

}
