package com.br.store.user;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.br.store.address.Address;
import com.br.store.email.address.EmailAddress;
import com.br.store.phone.Phone;
import com.br.store.serialize.AuthorityDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements UserDetails {

	private static final long serialVersionUID = 1L;

	/**
	 * User identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * User first name.
	 * <br/>
	 * Required with max length 255.
	 */
	@Column(name = "first_name", nullable = false, length = 255)
	private String firstName;

	/**
	 * User last name.
	 * <br/>
	 * Not required with max length 255.
	 * <br/>
	 */
	@Column(name = "last_name", nullable = true, length = 255)
	private String lastName;

	/**
	 * User date of birth.
	 * <br/>
	 * Required for employees, not required for customers.
	 * <br/>
	 * Must be in the past.
	 */
	@Column(name = "date_of_birth", nullable = true)
	private LocalDate dateOfBirth;

	/**
	 * User address.
	 */
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id", nullable = true)
	private Address address;

	/**
	 * List of email addresses.
	 */
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_email_address", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "email_address_id"))
	private List<EmailAddress> emails;
	
	/**
	 * List of phone numbers.
	 */
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_phone_number", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
	private List<Phone> phoneNumbers;

	/**
	 * User id.
	 */
	@Column(name = "username", nullable = false, length = 10, unique = true)
	private String username;
	
	/**
	 * User password.
	 */
	@Column(name = "password", nullable = false, length = 60)
	private String pwd;
	
	@ElementCollection(fetch = FetchType.EAGER)
    private List<String> roles = new ArrayList<>();
	
	@Override
	@JsonDeserialize(using = AuthorityDeserializer.class)
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@Override
	public String getPassword() {
		return this.getPwd();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
