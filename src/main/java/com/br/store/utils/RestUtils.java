package com.br.store.utils;

import java.nio.charset.StandardCharsets;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RestUtils {

	public static <T> ResponseEntity<T> send(String url, HttpMethod httpMethod, HttpEntity<Object> httpEntity, Class<T> returnClass) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		
		try {
			return restTemplate.exchange(url, httpMethod, httpEntity, returnClass);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static HttpEntity<Object> getHttpEntity() {
		HttpHeaders headers = new HttpHeaders();
		
		return new HttpEntity<Object>(headers);
	}
	
}
