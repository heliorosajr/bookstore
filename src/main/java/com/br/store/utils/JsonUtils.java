package com.br.store.utils;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonUtils {

	public static JsonObject parseToJson(String value) {
		if(value == null) {
			return null;
		}

		try {
			return JsonParser.parseString(value).getAsJsonObject();
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static String getJsonPropertyAsString(JsonObject json, String property) {
		if(json == null || json.get(property) instanceof JsonNull) {
			return null;
		}
		
		return json.get(property) != null ? json.get(property).getAsString() : null;
	}
	
}
