package com.br.store.cart.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.br.store.cart.Cart;
import com.br.store.exception.StoreException;

@Component
public class CartModelAssembler implements RepresentationModelAssembler<Cart, EntityModel<Cart>> {

	@Override
	public EntityModel<Cart> toModel(Cart cart) {
		try {
			List<Link> links = new ArrayList<>();
			if(cart.getId() != null) {
				links.add(linkTo(methodOn(CartController.class).findOne(cart.getId())).withSelfRel());
				links.add(linkTo(methodOn(CartController.class).update(cart, cart.getId())).withSelfRel());
				links.add(linkTo(methodOn(CartController.class).delete(cart.getId())).withSelfRel());
			}
			links.add(linkTo(methodOn(CartController.class).findAll()).withRel("find-all"));
			
			return EntityModel.of(cart, links);
		} catch (StoreException e) {
			e.printStackTrace();
		}
		return null;
	}

}
