package com.br.store.cart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.store.cart.Cart;
import com.br.store.cart.service.CartService;
import com.br.store.error.CustomError;
import com.br.store.exception.StoreException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/cart")
@Api(value = "Cart", tags = { "Cart" })
public class CartController {
	
	@Autowired
	private CartService cartService;

	@Autowired
	private CartModelAssembler assembler;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Cart list retrieved", response = Cart.class, responseContainer = "List"),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "List all carts", response = Cart.class, responseContainer = "List")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find-all", method = RequestMethod.GET)
	public CollectionModel<EntityModel<Cart>> findAll() throws StoreException {

		List<Cart> carts = cartService.findAll();

		return assembler.toCollectionModel(carts);
	}

	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Cart retrieved", response = Cart.class),
		    @ApiResponse(code = 404, message = "Cart not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Find cart by id", response = Cart.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public EntityModel<Cart> findOne(
		@ApiParam(name = "id", value = "Cart id") @PathVariable Long id) throws StoreException {

		Cart cart = cartService.findOne(id);
		
		return assembler.toModel(cart);
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "Cart created", response = Cart.class),
		    @ApiResponse(code = 400, message = "Bad request", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Create cart", response = Cart.class)
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(
		@ApiParam(name = "cart", value = "Cart data in JSON format") @RequestBody Cart cart) throws StoreException {

		EntityModel<Cart> entityModel = assembler.toModel(cartService.save(cart));

		return ResponseEntity
				.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(entityModel);
	}

	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Cart deleted"),
		    @ApiResponse(code = 404, message = "Cart not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Delete cart")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(
		@ApiParam(name = "id", value = "Cart id") @PathVariable Long id) throws StoreException {
		cartService.delete(id);
		
		return ResponseEntity.noContent().build();
	}
	
	// ----------------------------------------------------
	// Update
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Cart updated", response = Cart.class),
		    @ApiResponse(code = 404, message = "Cart not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Update Cart", response = Cart.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public EntityModel<Cart> update(
			@ApiParam(name = "cart", value = "Cart data in JSON format") @RequestBody Cart cart,
		@ApiParam(name = "id", value = "Cart id") @PathVariable Long id) throws StoreException {
		cart = cartService.update(cart, id);
		
		return assembler.toModel(cart);
	}
	
}
