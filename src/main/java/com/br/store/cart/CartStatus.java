package com.br.store.cart;

import lombok.Getter;

public enum CartStatus {
	
	PENDING("Pending"),
	COMPLETED("Completed"),
	CANCELED("Canceled");
	
	@Getter
	private String label;
	
	private CartStatus(String label) {
		this.label = label;
	}

}
