package com.br.store.cart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.cart.Cart;
import com.br.store.customer.Customer;

public interface CartRepository extends JpaRepository<Cart, Long> {
	
	public void deleteByCustomer(Customer customer);

}