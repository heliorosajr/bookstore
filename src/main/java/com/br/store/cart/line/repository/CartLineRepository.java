package com.br.store.cart.line.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.cart.line.CartLine;

public interface CartLineRepository extends JpaRepository<CartLine, Long> {

}