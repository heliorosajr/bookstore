package com.br.store.cart.line;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.br.store.book.Book;
import com.br.store.cart.Cart;

import lombok.Data;

@Entity
@Table(name = "cart_line")
@Data
public class CartLine {
	
	/**
	 * Cart line identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Book this cart line is associated to.
	 */
	@ManyToOne
	@JoinColumn(name = "book_id", nullable = true)
	private Book book;
	
	/**
	 * Number of items bought.
	 */
	@Column(name = "number_of_items", nullable = false)
	private int numberOfItems; 
	
	/**
	 * Contact this phone number is associated to.
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "cart_id", referencedColumnName = "id")
	private Cart cart;

}
