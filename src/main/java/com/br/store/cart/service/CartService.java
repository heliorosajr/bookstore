package com.br.store.cart.service;

import com.br.store.cart.Cart;
import com.br.store.crud.AbstractCrud;
import com.br.store.customer.Customer;
import com.br.store.exception.StoreException;

public interface CartService extends AbstractCrud<Cart> {
	
	// ----------------------------------------------------
	// Confirm or cancel cart
	// ----------------------------------------------------
	public Cart confirm(Cart cart) throws StoreException;
	
	public Cart cancel(Cart cart) throws StoreException;
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	public void deleteByCustomer(Customer customer) throws StoreException;
	
}
