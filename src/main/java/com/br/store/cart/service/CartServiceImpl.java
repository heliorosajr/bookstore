package com.br.store.cart.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.book.Book;
import com.br.store.book.service.BookService;
import com.br.store.cart.Cart;
import com.br.store.cart.CartStatus;
import com.br.store.cart.line.CartLine;
import com.br.store.cart.repository.CartRepository;
import com.br.store.customer.Customer;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	private CartRepository repository;

	@Autowired
	private BookService bookService;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public Cart findOne(Long id) throws StoreException {
		try {
			Optional<Cart> cart = repository.findById(id);
			
			if(cart.isPresent()) {
				return cart.get();
			}
			
			throw new StoreException(ErrorDomain.CART, ErrorCode.ENTITY_NOT_FOUND, "Entity not found", "Could not retrieve cart with id " + id, HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Error retrieving cart with id " + id, e);
		}
	}
	

	@Override
	public List<Cart> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not retrieve carts", e);
		}
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public Cart save(Cart cart) throws StoreException {
		try {
			return repository.save(cart);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not save cart", e);
		}
	}

	@Override
	public List<Cart> saveAll(List<Cart> carts) throws StoreException {
		try {
			return repository.saveAll(carts);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not save carts", e);
		}
	}
	
	@Override
	public Cart update(Cart cart, Long id) throws StoreException {
		try {
			findOne(id);
			return save(cart);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not update cart with id " + id, e);
		}
	}

	// ----------------------------------------------------
	// Confirm or cancel
	// ----------------------------------------------------
	@Override
	public Cart confirm(Cart cart) throws StoreException {
		Cart cartFromDb = null;

		try {
			cartFromDb = findOne(cart.getId());
			List<Book> books = processBooksFromCart(cart);

			bookService.saveAll(books);

			return repository.save(cartFromDb);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not confirm cart " + cart.getId(), e);
		}
	}
	
	private List<Book> processBooksFromCart(Cart cart) throws StoreException {
		List<Book> books = new ArrayList<>();
		int total = 0;
		
		for(CartLine cl : cart.getCartLines()) {
			Book book = bookService.findOne(cl.getBook().getId());
			total = book.getQuantity() - cl.getNumberOfItems();
			if(total < 0) {
				book.setQuantity(total);
			}
			books.add(book);
		}
		
		return books;
	}
	
	@Override
	public Cart cancel(Cart cart) throws StoreException {
		Cart cartFromDb = null;

		try {
			cartFromDb = findOne(cart.getId());
			cartFromDb.setStatus(CartStatus.CANCELED);

			return repository.save(cartFromDb);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not cancel cart " + cart.getId(), e);
		}
	}
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			Cart cart = findOne(id);
			repository.delete(cart);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.BOOK, ErrorCode.SERVER_ERROR, "Could not delete cart with id " + id, e);
		}
	}
	
	@Override
	public void deleteByCustomer(Customer customer) throws StoreException {
		try {
			repository.deleteByCustomer(customer);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CART, ErrorCode.SERVER_ERROR, "Could not delete carts for customer " + customer.getId(), e);
		}
	}

	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(Cart cart) throws StoreException {

	}

}
