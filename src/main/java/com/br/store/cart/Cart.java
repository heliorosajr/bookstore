package com.br.store.cart;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.br.store.cart.line.CartLine;
import com.br.store.credit.card.CreditCard;
import com.br.store.customer.Customer;

import lombok.Data;

@Entity
@Table(name = "cart")
@Data
public class Cart {
	
	/**
	 * Cart identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Customer this cart is associated to.
	 */
	@ManyToOne
	@JoinColumn(name = "customer_id", nullable = true)
	private Customer customer;
	
	/**
	 * Customer this cart is associated to.
	 */
	@ManyToOne
	@JoinColumn(name = "credit_card_id", nullable = true)
	private CreditCard creditCard;
	
	/**
	 * List of items in cart.
	 */
	@OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CartLine> cartLines;
	
	/**
	 * Cart status.
	 */
	@Enumerated(EnumType.STRING)
	private CartStatus status;
	
}
