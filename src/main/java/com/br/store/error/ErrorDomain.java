package com.br.store.error;

import lombok.Getter;

public enum ErrorDomain {
	
	AUTHENTICATION("authentication"),
	BANK("bank"),
	CART("cart"),
	CITY("city"),
	CUSTOMER("customer"),
	EMPLOYEE("employee"),
	BOOK("book"),
	USER("user"),
	STATE("state");
	
	@Getter
	private String domain;
	
	private ErrorDomain(String domain) {
		this.domain = domain;
	}

}
