package com.br.store.error;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.br.store.exception.StoreException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomError {
	
	public CustomError(final HttpStatus status, final Exception ex, final List<String> errors) {
        super();
        loadDetails(ex);
        this.status = status;
        this.errors = errors;
    }

    public CustomError(final HttpStatus status, final Exception ex, final String error) {
        super();
        loadDetails(ex);
        this.status = status;
    }
    
    private void loadDetails(Exception ex) {
    	errorMessage = ex.getMessage();
    	
    	if(ex instanceof StoreException) {
    		errorDetails = ((StoreException) ex).getDetails();
    		errorCode = ((StoreException) ex).getErrorCode();
    		errorDomain = ((StoreException) ex).getErrorDomain();
		}
    }
	
	@JsonProperty(index = 0)
	private String errorMessage;
	
	@JsonProperty(index = 1)
	private String errorDetails;
	
	@JsonProperty(index = 2)
	private ErrorCode errorCode;
	
	@JsonProperty(index = 3)
	private ErrorDomain errorDomain;
	
	@JsonProperty(index = 4)
	private List<String> errors;
	
	@JsonProperty(index = 5)
	private HttpStatus status;
	
	@JsonProperty(index = 6)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime timestamp;
	
}
