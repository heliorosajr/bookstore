package com.br.store.error;

public enum ErrorCode {
	
	AUTHENTICATION_FAILED,
	BAD_REQUEST,
	ENTITY_NOT_FOUND,
	SERVER_ERROR;
	
	private ErrorCode() {}
	
}
