package com.br.store.email.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "email_address")
@Data
public class EmailAddress {
	
	/**
	 * Email identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Email address.
	 * <br/>
	 * Max length 255.
	 */
	@Column(name = "email", nullable = true, length = 255)
	private String email;
	
	/**
	 * Phone number description.
	 * <br/>
	 * Max length 100.
	 */
	@Column(name = "description", nullable = true, length = 100)
	private String description;
	
}