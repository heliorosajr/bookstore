package com.br.store.email.address.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.email.address.EmailAddress;

public interface EmailAddressRepository extends JpaRepository<EmailAddress, Long> {
	
}