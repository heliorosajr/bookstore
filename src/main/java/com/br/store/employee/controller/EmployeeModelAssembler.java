package com.br.store.employee.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.br.store.employee.Employee;
import com.br.store.exception.StoreException;

@Component
public class EmployeeModelAssembler implements RepresentationModelAssembler<Employee, EntityModel<Employee>> {
	
	@Override
	public EntityModel<Employee> toModel(Employee employee) {
		try {
			List<Link> links = new ArrayList<>();
			if(employee.getId() != null) {
				links.add(linkTo(methodOn(EmployeeController.class).findOne(employee.getId())).withSelfRel());
				links.add(linkTo(methodOn(EmployeeController.class).update(employee, employee.getId())).withSelfRel());
				links.add(linkTo(methodOn(EmployeeController.class).delete(employee.getId())).withSelfRel());
			}
			links.add(linkTo(methodOn(EmployeeController.class).findAll()).withRel("find-all"));
			
			return EntityModel.of(employee, links);
		} catch (StoreException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}