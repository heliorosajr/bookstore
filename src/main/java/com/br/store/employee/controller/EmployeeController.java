package com.br.store.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.store.employee.Employee;
import com.br.store.employee.service.EmployeeService;
import com.br.store.error.CustomError;
import com.br.store.exception.StoreException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/employee")
@Api(value = "Employee", tags = { "Employee" })
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private EmployeeModelAssembler assembler;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Employee list retrieved", response = Employee.class, responseContainer = "List"),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "List all employees", response = Employee.class, responseContainer = "List")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find-all", method = RequestMethod.GET)
	public CollectionModel<EntityModel<Employee>> findAll() throws StoreException {

		List<Employee> employees = employeeService.findAll();

		return assembler.toCollectionModel(employees);
	}

	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Employee retrieved", response = Employee.class),
		    @ApiResponse(code = 404, message = "Employee not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Find employee by id", response = Employee.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public EntityModel<Employee> findOne(
		@ApiParam(name = "id", value = "Employee id") @PathVariable Long id) throws StoreException {
		
		Employee employee = employeeService.findOne(id);
		
		return assembler.toModel(employee);
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "Employee created", response = Employee.class),
		    @ApiResponse(code = 400, message = "Bad request", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Create employee", response = Employee.class)
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(
		@ApiParam(name = "employee", value = "Employee data in JSON format") @RequestBody Employee employee) throws StoreException {

		EntityModel<Employee> entityModel = assembler.toModel(employeeService.save(employee));

		return ResponseEntity
				.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(entityModel);
	}

	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Employee deleted"),
		    @ApiResponse(code = 404, message = "Employee not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Delete employee")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(
		@ApiParam(name = "id", value = "Employee id") @PathVariable Long id) throws StoreException {
		employeeService.delete(id);
		
		return ResponseEntity.ok().build();
	}
	
	// ----------------------------------------------------
	// Update
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Employee updated", response = Employee.class),
		    @ApiResponse(code = 404, message = "Employee not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Update employee", response = Employee.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public EntityModel<Employee> update(
		@ApiParam(name = "employee", value = "Employee data in JSON format") @RequestBody Employee employee,
		@ApiParam(name = "id", value = "Employee id") @PathVariable Long id) throws StoreException {
		employee = employeeService.update(employee, id);
		
		return assembler.toModel(employee);
	}

}
