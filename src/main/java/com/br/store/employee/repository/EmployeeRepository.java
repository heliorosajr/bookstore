package com.br.store.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.employee.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	public Employee findByEmail(String email);
	
}
