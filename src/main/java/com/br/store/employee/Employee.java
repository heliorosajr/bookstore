package com.br.store.employee;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.br.store.user.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "employee")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EqualsAndHashCode(callSuper=true)
public class Employee extends User {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Employee corporate email.
	 * <br/>
	 * Required and unique.
	 */
	@Column(name = "email", nullable = false, length = 255, unique = true)
	private String email;

	/**
	 * Employee date of hiring.
	 * <br/>
	 * Required.
	 */
	@Column(name = "date_of_hiring", nullable = false)
	private LocalDate dateOfHiring;
	
	/**
	 * Employee date of firing.
	 * <br/>
	 * Not required.
	 */
	@Column(name = "date_of_firing")
	private LocalDate dateOfFiring;
	
	/**
	 * Employee salary.
	 * <br/>
	 * Required.
	 */
	@Column(name = "salary", nullable = false)
	private BigDecimal salary;
	
}
