package com.br.store.employee.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.crud.AbstractCrud;
import com.br.store.employee.Employee;
import com.br.store.employee.repository.EmployeeRepository;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.user.User;
import com.br.store.user.service.UserService;
import com.br.store.utils.Constants;
import com.br.store.utils.ValidationUtils;

@Transactional
@Service
public class EmployeeService implements AbstractCrud<Employee> {

	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
	private UserService userService;
	
	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public Employee findOne(Long id) throws StoreException {
		try {
			Optional<Employee> employee = repository.findById(id);
			
			if(employee.isPresent()) {
				return employee.get();
			}
			
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND,
				Constants.ENTITY_NOT_FOUND, String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", id), HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Error retrieving employee with id " + id, e);
		}
	}
	
	@Override
	public List<Employee> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Could not retrieve employees", e);
		}
	}
	
	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public Employee save(Employee employee) throws StoreException {
		try {
			validate(employee);
			return repository.save(employee);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Could not save employee", e);
		}
	}
	
	@Override
	public List<Employee> saveAll(List<Employee> employees) throws StoreException {
		try {
			return repository.saveAll(employees);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Could not save employees", e);
		}
	}
	
	@Override
	public Employee update(Employee employee, Long id) throws StoreException {
		try {
			findOne(id);
			return save(employee);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Could not update employee with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			Employee employee = findOne(id);
			repository.delete(employee);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR,
				"Could not delete employee with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(Employee employee) throws StoreException {
		userService.validate(employee);
		
		// email
		User employeeFromDb = repository.findByEmail(employee.getEmail());
		if(employeeFromDb != null && !employeeFromDb.getId().equals(employee.getId())) {
			throw new StoreException(ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST, Constants.INVALID_VALUES,
				"Email already in use", HttpStatus.BAD_REQUEST);
		}
		ValidationUtils.checkIfEmpty(employee.getEmail(), ErrorDomain.EMPLOYEE, "Employee email address");
		ValidationUtils.checkIfExceeds(employee.getEmail(), 255, ErrorDomain.EMPLOYEE, "Employee email address");
		ValidationUtils.isEmailValid(employee.getEmail(), ErrorDomain.EMPLOYEE, "Employee email address");
		
		// date of hiring
		ValidationUtils.checkIfEmpty(employee.getDateOfHiring(), ErrorDomain.EMPLOYEE, "Employee date of hiring");
		
		// salary
		ValidationUtils.checkIfEmpty(employee.getSalary(), ErrorDomain.EMPLOYEE, "Employee salary");
		ValidationUtils.checkIfNegative(employee.getSalary(), ErrorDomain.EMPLOYEE, "Employee salary");
	}

}
