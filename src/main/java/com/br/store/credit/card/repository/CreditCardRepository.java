package com.br.store.credit.card.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.credit.card.CreditCard;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {

}