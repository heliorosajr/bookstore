package com.br.store.credit.card;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.br.store.customer.Customer;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table(name = "credit_card")
@Data
public class CreditCard {
	
	/**
	 * Credit card identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Credit card number.
	 * <br/>
	 * Required with max length 500.
	 */
	@Column(name = "number", nullable = false, length = 16)
	private String number;
	
	/**
	 * Credit card name.
	 * <br/>
	 * Required with max length 255.
	 */
	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	/**
	 * Credit card expiration.
	 * <br/>
	 * Required.
	 */
	@Column(name = "expiration", nullable = false)
	private LocalDate expiration;
	
	/**
	 * Customer this credit card is associated to.
	 */
	@JsonBackReference
	@ManyToOne(optional = false)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

}
