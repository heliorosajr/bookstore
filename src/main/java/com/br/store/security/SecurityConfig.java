package com.br.store.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
   
	@Autowired
    TokenProvider tokenProvider;
	
	private static final String[] AUTH_WHITELIST = {
		// -- Swagger UI v2
		"/v2/api-docs",
		"/swagger-resources",
		"/swagger-resources/**",
		"/configuration/ui",
		"/configuration/security",
		"/swagger-ui.html",
		"/webjars/**",
		// -- Swagger UI v3 (OpenAPI)
		"/v3/api-docs/**",
		"/swagger-ui/**"
	};
    
	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
    protected void configure(HttpSecurity http) throws Exception {
        http
            .httpBasic().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers("/auth/signin").permitAll()
                .antMatchers(HttpMethod.GET, "/employee/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.POST, "/employee/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.PUT, "/employee/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.DELETE, "/employee/**").hasRole("EMPLOYEE")
                
                .antMatchers(HttpMethod.GET, "/customer/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.POST, "/customer/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.PUT, "/customer/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.DELETE, "/customer/**").hasRole("EMPLOYEE")
                
                .antMatchers(HttpMethod.GET, "/book/**").permitAll()
                .antMatchers(HttpMethod.POST, "/book/**").hasAnyRole("EMPLOYEE")
                .antMatchers(HttpMethod.PUT, "/book/**").hasAnyRole("EMPLOYEE")
                .antMatchers(HttpMethod.DELETE, "/book/**").hasAnyRole("EMPLOYEE")
                
                .antMatchers(HttpMethod.GET, "/cart/**").permitAll()
                .antMatchers(HttpMethod.POST, "/cart/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/cart/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/cart/**").permitAll()
                
                .antMatchers(HttpMethod.GET, "/swagger-ui/*").permitAll()
                .anyRequest().authenticated()
            .and()
            .apply(new JwtConfigurer(tokenProvider));
    }
}