package com.br.store.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.br.store.book.service.BookService;
import com.br.store.cart.service.CartService;
import com.br.store.city.service.CityService;
import com.br.store.customer.service.CustomerService;
import com.br.store.employee.service.EmployeeService;
import com.br.store.state.service.StateService;

@Configuration
class DatabaseConfig {

	private static final Logger log = LoggerFactory.getLogger(DatabaseConfig.class);
	
	@Bean
	CommandLineRunner initBooks(BookService bookService, CustomerService customerService,
			EmployeeService employeeService, CityService cityService, StateService stateService,
			CartService cartService) {
		return args -> {
			// remove comment to force mocking
//			MockUtils.mockEntities(bookService, customerService, employeeService, cityService,
//				stateService, cartService);
			log.info("Entities mocked");
		};
	}
	
}