package com.br.store.state.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.state.State;

public interface StateRepository extends JpaRepository<State, Long> {
	
	public State findByNameAndInitials(String name, String initials);
	
}