package com.br.store.state;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Table(name = "state", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "initials" }))
@Data
public class State {
	
	/**
	 * State identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * State name.
	 * <br/>
	 * Required with max length 255.
	 */
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	/**
	 * State initials.
	 * <br/>
	 * Required with max length 2.
	 */
	@Column(name = "initials", nullable = false, length = 2)
	private String initials;
	
}
