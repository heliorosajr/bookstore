package com.br.store.state.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.crud.AbstractCrud;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.State;
import com.br.store.state.repository.StateRepository;
import com.br.store.utils.Constants;
import com.br.store.utils.ValidationUtils;

@Service
public class StateService implements AbstractCrud<State> {

	@Autowired
	private StateRepository repository;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public State findOne(Long id) throws StoreException {
		try {
			Optional<State> state = repository.findById(id);

			if(state.isPresent()) {
				return state.get();
			}

			throw new StoreException(ErrorDomain.STATE, ErrorCode.ENTITY_NOT_FOUND,
				Constants.ENTITY_NOT_FOUND, String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "state", id), HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Error retrieving state with id " + id, e);
		}
	}

	@Override
	public List<State> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Could not retrieve states", e);
		}
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public State save(State state) throws StoreException {
		try {
			State stateFromDb = repository.findByNameAndInitials(state.getName(), state.getInitials());
			if(stateFromDb != null) {
				return stateFromDb;
			}

			validate(state);
			return repository.save(state);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Could not save state", e);
		}
	}
	
	@Override
	public List<State> saveAll(List<State> states) throws StoreException {
		try {
			return repository.saveAll(states);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Could not save states", e);
		}
	}
	
	@Override
	public State update(State state, Long id) throws StoreException {
		try {
			findOne(id);
			State stateFromDb = repository.findByNameAndInitials(state.getName(), state.getInitials());
			if(stateFromDb != null) {
				throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
						"Could not update state with id " + id, "State already exists", HttpStatus.BAD_REQUEST);
			}
			
			return save(state);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Could not update state with id " + id, e);
		}
	}

	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			State state = findOne(id);
			repository.delete(state);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.SERVER_ERROR,
				"Could not delete state with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(State state) throws StoreException {
		// state initials
		ValidationUtils.checkIfEmpty(state.getInitials(), ErrorDomain.STATE, "State initials");
		if(state.getInitials().length() != 2) {
			throw new StoreException(ErrorDomain.STATE, ErrorCode.BAD_REQUEST,
				Constants.INVALID_VALUES, Constants.STATE_INITIALS_LENGTH, HttpStatus.BAD_REQUEST);
		}
		
		// state name
		ValidationUtils.checkIfEmpty(state.getName(), ErrorDomain.STATE, "State name");
		ValidationUtils.checkIfExceeds(state.getName(), 255, ErrorDomain.STATE, "State name");
	}
	
}
