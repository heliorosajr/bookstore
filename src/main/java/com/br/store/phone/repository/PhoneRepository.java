package com.br.store.phone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.phone.Phone;

public interface PhoneRepository extends JpaRepository<Phone, Long> {
	
}