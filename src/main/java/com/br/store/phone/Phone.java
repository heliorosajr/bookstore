package com.br.store.phone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "phone_number")
@Data
public class Phone {
	
	/**
	 * Phone number identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Phone number.
	 * <br/>
	 * Max length 15.
	 */
	@Column(name = "number", nullable = true, length = 15)
	private String number;
	
	/**
	 * Phone number description.
	 * <br/>
	 * Max length 100.
	 */
	@Column(name = "description", nullable = true, length = 100)
	private String description;

}
