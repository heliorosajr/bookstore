package com.br.store.exception;

import org.springframework.http.HttpStatus;

import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;

import lombok.Getter;

public class StoreException extends Exception {
	
	private static final long serialVersionUID = 1L;

	@Getter
	private String details;
	
	@Getter
	private ErrorCode errorCode;
	
	@Getter
	private ErrorDomain errorDomain;
	
	@Getter
	private HttpStatus status;
	
	public StoreException(ErrorDomain errorDomain, ErrorCode errorCode, String message, String details, HttpStatus status) {
		super(message);
		this.details = details;
		this.errorCode = errorCode;
		this.errorDomain = errorDomain;
		this.status = status;
	}
	
	public StoreException(ErrorDomain errorDomain, ErrorCode errorCode, String message, String details) {
		this(errorDomain, errorCode, message, details, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public StoreException(ErrorDomain errorDomain, ErrorCode errorCode, String message, Exception exception, HttpStatus status) {
		this(errorDomain, errorCode, message, ExceptionUtils.getMessage(exception), status);
	}
	
	public StoreException(ErrorDomain errorDomain, ErrorCode errorCode, String message, Exception exception) {
		this(errorDomain, errorCode, message, ExceptionUtils.getMessage(exception), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
