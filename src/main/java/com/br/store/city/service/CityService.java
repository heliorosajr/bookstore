package com.br.store.city.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.city.City;
import com.br.store.city.repository.CityRepository;
import com.br.store.crud.AbstractCrud;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.State;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.ValidationUtils;

@Service
public class CityService implements AbstractCrud<City> {
	
	@Autowired
	private CityRepository repository;
	
	@Autowired
	private StateService stateService;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public City findOne(Long id) throws StoreException {
		try {
			Optional<City> city = repository.findById(id);
			
			if(city.isPresent()) {
				return city.get();
			}
			
			throw new StoreException(ErrorDomain.CITY, ErrorCode.ENTITY_NOT_FOUND,
					Constants.ENTITY_NOT_FOUND, String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "city", id), HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Error retrieving city with id " + id,  e);
		}
	}
	
	@Override
	public List<City> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Could not retrieve cities", e);
		}
	}
	
	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public City save(City city) throws StoreException {
		try {
			if(city.getState() != null && city.getState().getId() != null) {
				City cityFromDb = repository.findByNameAndState(city.getName(), city.getState());
				if(cityFromDb != null) {
					return cityFromDb;
				}
			}
			
			validate(city);
			return repository.save(city);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Could not save city", e);
		}
	}
	
	@Override
	public List<City> saveAll(List<City> cities) throws StoreException {
		try {
			return repository.saveAll(cities);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Could not save cities", e);
		}
	}
	
	@Override
	public City update(City city, Long id) throws StoreException {
		try {
			findOne(id);
			City cityFromDb = repository.findByNameAndState(city.getName(), city.getState());
			if(cityFromDb != null) {
				throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
						"Could not update city with id " + id, "City already exists", HttpStatus.BAD_REQUEST);
			}

			return save(city);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Could not update city with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			City city = findOne(id);
			repository.delete(city);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CITY, ErrorCode.SERVER_ERROR,
				"Could not delete city with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(City city) throws StoreException {
		// city name
		ValidationUtils.checkIfEmpty(city.getName(), ErrorDomain.CITY, "City name");
		ValidationUtils.checkIfExceeds(city.getName(), 255, ErrorDomain.CITY, "City name");
		
		// if State does not exist, it will be created
		if(city.getState() != null) {
			State state = stateService.save(city.getState());
			city.setState(state);
		}

		// city state
		ValidationUtils.checkIfEmpty(city.getState(), ErrorDomain.CITY, "State");
	}
	
}