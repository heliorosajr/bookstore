package com.br.store.city.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.city.City;
import com.br.store.state.State;

public interface CityRepository extends JpaRepository<City, Long> {
	
	public City findByNameAndState(String name, State state);

}