package com.br.store.city;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.br.store.state.State;

import lombok.Data;

@Entity
@Table(name = "city", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "state_id" }))
@Data
public class City {
	
	/**
	 * City identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * City name.
	 * <br/>
	 * Required with max length 255.
	 */
	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	/**
	 * The state the city belongs.
	 */
	@ManyToOne
	@JoinColumn(name = "state_id", nullable = false)
	private State state;
	

}
