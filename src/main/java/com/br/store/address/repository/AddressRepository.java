package com.br.store.address.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.address.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
