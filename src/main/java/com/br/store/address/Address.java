package com.br.store.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.br.store.city.City;

import lombok.Data;

@Entity
@Table(name = "address")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class Address {
	
	/**
	 * Address identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Address information.
	 * <br/>
	 * Required with max length 500.
	 */
	@Column(name = "address_description", nullable = false, length = 500)
	private String addressInformation;
	
	/**
	 * Address CEP.
	 * <br/>
	 * Required with max length 8.
	 */
	@Column(name = "cep", nullable = false, length = 8)
	private String cep;
	
	/**
	 * The city the address belongs.
	 */
	@ManyToOne
	@JoinColumn(name = "city_id", nullable = false)
	private City city;
	
}
