package com.br.store.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.store.customer.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
}
