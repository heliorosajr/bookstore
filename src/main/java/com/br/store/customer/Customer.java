package com.br.store.customer;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.br.store.credit.card.CreditCard;
import com.br.store.user.User;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "customer")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EqualsAndHashCode(callSuper=true)
public class Customer extends User {
	
	private static final long serialVersionUID = 1L;

	/**
	 * User date of registration.
	 */
	@Column(name = "date_of_registration", nullable = false)
	private LocalDate dateOfRegistration;

	/**
	 * List of credit cards.
	 */
	@JsonManagedReference
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CreditCard> creditCards;

}
