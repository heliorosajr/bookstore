package com.br.store.customer.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.br.store.customer.Customer;
import com.br.store.exception.StoreException;

@Component
public class CustomerModelAssembler implements RepresentationModelAssembler<Customer, EntityModel<Customer>> {

	@Override
	public EntityModel<Customer> toModel(Customer customer) {
		try {
			List<Link> links = new ArrayList<>();
			if(customer.getId() != null) {
				links.add(linkTo(methodOn(CustomerController.class).findOne(customer.getId())).withSelfRel());
				links.add(linkTo(methodOn(CustomerController.class).update(customer, customer.getId())).withSelfRel());
				links.add(linkTo(methodOn(CustomerController.class).delete(customer.getId())).withSelfRel());
			}
			links.add(linkTo(methodOn(CustomerController.class).findAll()).withRel("find-all"));
			
			return EntityModel.of(customer, links);
		} catch (StoreException e) {
			e.printStackTrace();
		}
		return null;
	}

}
