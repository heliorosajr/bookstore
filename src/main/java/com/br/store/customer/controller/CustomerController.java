package com.br.store.customer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.error.CustomError;
import com.br.store.exception.StoreException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/customer")
@Api(value = "Customer", tags = { "Customer" })
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerModelAssembler assembler;

	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Customer list retrieved", response = Customer.class, responseContainer = "List"),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "List all customers", response = Customer.class, responseContainer = "List")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find-all", method = RequestMethod.GET)
	public CollectionModel<EntityModel<Customer>> findAll() throws StoreException {

		List<Customer> customers = customerService.findAll();

		return assembler.toCollectionModel(customers);
	}

	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Customer retrieved", response = Customer.class),
		    @ApiResponse(code = 404, message = "Customer not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Find customer by id", response = Customer.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public EntityModel<Customer> findOne(
			@ApiParam(name = "id", value = "Customer id") @PathVariable Long id) throws StoreException {

		Customer customer = customerService.findOne(id);
		
		return assembler.toModel(customer);
	}

	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "Customer created", response = Customer.class),
		    @ApiResponse(code = 400, message = "Bad request", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Create customer", response = Customer.class)
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(
		@ApiParam(name = "customer", value = "Customer data in JSON format") @RequestBody Customer customer) throws StoreException {

		EntityModel<Customer> entityModel = assembler.toModel(customerService.save(customer));

		return ResponseEntity
				.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(entityModel);
	}

	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Customer deleted"),
		    @ApiResponse(code = 404, message = "Customer not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Delete customer")
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(
		@ApiParam(name = "id", value = "Customer id") @PathVariable Long id) throws StoreException {
		customerService.delete(id);
		
		return ResponseEntity.ok().build();
	}
	
	// ----------------------------------------------------
	// Update
	// ----------------------------------------------------
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Customer updated", response = Customer.class),
		    @ApiResponse(code = 404, message = "Customer not found", response = CustomError.class),
		    @ApiResponse(code = 500, message = "Internal Server Error", response = CustomError.class)
		})
	@ApiOperation(value = "Update customer", response = Customer.class)
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public EntityModel<Customer> update(
		@ApiParam(name = "customer", value = "Customer data in JSON format") @RequestBody Customer customer,
		@ApiParam(name = "id", value = "Customer id") @PathVariable Long id) throws StoreException {
		customer = customerService.update(customer, id);
		
		return assembler.toModel(customer);
	}

}
