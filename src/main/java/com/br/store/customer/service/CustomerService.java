package com.br.store.customer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.br.store.cart.service.CartService;
import com.br.store.credit.card.CreditCard;
import com.br.store.crud.AbstractCrud;
import com.br.store.customer.Customer;
import com.br.store.customer.repository.CustomerRepository;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.user.service.UserService;
import com.br.store.utils.Constants;
import com.br.store.utils.ValidationUtils;

@Service
public class CustomerService implements AbstractCrud<Customer> {

	@Autowired
	private CustomerRepository repository;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private UserService userService;
	
	// ----------------------------------------------------
	// Read
	// ----------------------------------------------------
	@Override
	public Customer findOne(Long id) throws StoreException {
		try {
			Optional<Customer> customer = repository.findById(id);

			if(customer.isPresent()) {
				return customer.get();
			}

			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND,
				Constants.ENTITY_NOT_FOUND, String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", id), HttpStatus.NOT_FOUND);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Error retrieving customer with id " + id, e);
		}
	}
	
	@Override
	public List<Customer> findAll() throws StoreException {
		try {
			return repository.findAll();
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Could not retrieve customers", e);
		}
	}
	
	// ----------------------------------------------------
	// Persist
	// ----------------------------------------------------
	@Override
	public Customer save(Customer customer) throws StoreException {
		try {
			validate(customer);
			return repository.save(customer);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Could not save customer", e);
		}
	}
	
	@Override
	public List<Customer> saveAll(List<Customer> customers) throws StoreException {
		try {
			return repository.saveAll(customers);
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Could not save customers", e);
		}
	}
	
	@Override
	public Customer update(Customer customer, Long id) throws StoreException {
		try {
			findOne(id);
			return save(customer);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Could not update customer with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Delete
	// ----------------------------------------------------
	@Override
	public void delete(Long id) throws StoreException {
		try {
			Customer customer = findOne(id);
			cartService.deleteByCustomer(customer);
			repository.delete(customer);
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR,
				"Could not delete customer with id " + id, e);
		}
	}
	
	// ----------------------------------------------------
	// Validation
	// ----------------------------------------------------
	@Override
	public void validate(Customer customer) throws StoreException {
		userService.validate(customer);
		
		ValidationUtils.checkIfEmpty(customer.getDateOfRegistration(), ErrorDomain.CUSTOMER, "Customer date of registration");
		ValidationUtils.checkIfDateIsInPast(customer.getDateOfRegistration(), ErrorDomain.CUSTOMER, "Customer date of registration");
		
		validateCreditCards(customer);
	}
	
	private void validateCreditCards(Customer customer) throws StoreException {
		for(CreditCard cc : customer.getCreditCards()) {
			ValidationUtils.checkIfEmpty(cc.getNumber(), ErrorDomain.CUSTOMER, "Customer credit card number");
			ValidationUtils.checkIfExceeds(cc.getNumber(), 16, ErrorDomain.CUSTOMER, "Customer credit card number");
			
			ValidationUtils.checkIfEmpty(cc.getName(), ErrorDomain.CUSTOMER, "Customer credit card name");
			ValidationUtils.checkIfExceeds(cc.getName(), 255, ErrorDomain.CUSTOMER, "Customer credit card name");
			
			ValidationUtils.checkIfEmpty(cc.getExpiration(), ErrorDomain.CUSTOMER, "Customer credit card expiration");
		}
	}

}