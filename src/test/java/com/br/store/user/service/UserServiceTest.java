package com.br.store.user.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.employee.Employee;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.service.StateService;
import com.br.store.user.User;
import com.br.store.utils.Constants;
import com.br.store.utils.MockUtils;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=userDb","store.trx.datasource.url=jdbc:h2:mem:userDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class UserServiceTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private CustomerService customerService;
	
	@Test
	@DisplayName("Test if exception is thrown when an username is not found")
	public void findInexistentUser() throws StoreException {
		final String username = RandomStringUtils.random(10);
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.findByUsername(username);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_GENERIC_MESSAGE, "user", "username", username),
			ErrorDomain.USER, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if existing user is found by username")
	public void findUserByUsername() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername("cust001");
		customer.setPwd(MockUtils.getPassword(false));
		customerService.save(customer);
		
		User user = userService.findByUsername(customer.getUsername());
		
		assertEquals(user.getId(), customer.getId());
		assertEquals(user.getUsername(), customer.getUsername());
	}
	
	@Test
	@DisplayName("Test if an user without first name is persisted")
	public void persistUserWithoutFirstName() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setFirstName(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "User first name"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with empty first name is persisted")
	public void persistUserWithEmptyFirstName() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setFirstName("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "User first name"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with first name that exceeds limit is persisted")
	public void persistUserWithFirstNameExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setFirstName(RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User first name", 255), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with last name that exceeds limit is persisted")
	public void persistUserWithLastNameExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setLastName(RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User last name", 255), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee without date of birth is persisted")
	public void persistEmployeeWithoutDateOfBirth() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setDateOfBirth(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee date of birth"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with date of birth in future is persisted")
	public void persistUserWithDateOfBirthInFuture() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setDateOfBirth(LocalDate.now().plusDays(10));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.DATE_ERROR, "User date of birth", "past"), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee without address is persisted")
	public void persistEmployeeWithoutAddressOfBirth() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setAddress(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee address"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address without address information is persisted")
	public void persistUserAddressWithoutAddressInformation() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setAddressInformation(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Address information"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address with empty address information is persisted")
	public void persistUserAddressWithEmptyAddressInformation() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setAddressInformation("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Address information"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address with address information that exceeds limit is persisted")
	public void persistUserAddressWithAddressInformationExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setAddressInformation(RandomStringUtils.randomAlphabetic(550));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Address information", 500), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address without CEP is persisted")
	public void persistUserAddressWithoutCEP() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setCep(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Address CEP"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address with empty CEP is persisted")
	public void persistUserAddressWithEmptyCEP() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setCep("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Address CEP"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address with CEP that exceeds limit is persisted")
	public void persistUserAddressWithCEPExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setCep(RandomStringUtils.randomAlphabetic(15));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Address CEP", 8), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user address without city is persisted")
	public void persistUserAddressWithoutCity() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getAddress().setCity(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Address city"), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user contact email that exceeds limit is persisted")
	public void persistUserContactEmailExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getEmails().get(0).setEmail(RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User email address", 255), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user invalid contact email is persisted")
	public void persistUserWithInvalidContactEmail() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getEmails().get(0).setEmail("email");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMAIL_ERROR, "email"), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user phone description that exceeds limit is persisted")
	public void persistUserEmailDescriptionExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getEmails().get(0).setDescription(RandomStringUtils.randomAlphabetic(200));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User email description", 100), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user phone number that exceeds limit is persisted")
	public void persistUserPhoneNumberExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getPhoneNumbers().get(0).setNumber(RandomStringUtils.randomAlphabetic(20));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User phone number", 15), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user phone description that exceeds limit is persisted")
	public void persistUserPhoneDescriptionExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getPhoneNumbers().get(0).setDescription(RandomStringUtils.randomAlphabetic(200));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User phone number description", 100), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user without username is persisted")
	public void persistUserWithoutUsername() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Username"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with empty username is persisted")
	public void persistUserWithEmptyUsername() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Username"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user username that exceeds limit is persisted")
	public void persistUserUsernamelExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername(RandomStringUtils.randomAlphabetic(100));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Username", 60), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user without password is persisted")
	public void persistUserWithoutPwd() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setPwd(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "User password"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with empty password is persisted")
	public void persistUserWithEmptyPwd() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setPwd("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "User password"),
			ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user password that exceeds limit is persisted")
	public void persistUserPwdExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setPwd(RandomStringUtils.randomAlphabetic(100));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "User password", 60), ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an user with repeated username is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistUserWithRepeatedUsername() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername("username1");
		customerService.save(customer);
		
		Customer customerToTest = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customerToTest.setUsername("username1");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			userService.validate(customerToTest);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			"Username already in use", ErrorDomain.USER, ErrorCode.BAD_REQUEST);
	}

}
