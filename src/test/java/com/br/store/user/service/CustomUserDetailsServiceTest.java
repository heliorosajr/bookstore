package com.br.store.user.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.exception.StoreException;
import com.br.store.state.service.StateService;
import com.br.store.user.User;
import com.br.store.utils.MockUtils;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=customUserDb","store.trx.datasource.url=jdbc:h2:mem:customUserDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CustomUserDetailsServiceTest {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private CustomerService customerService;
	
	@Test
	@DisplayName("Test if exception is thrown when an username is not found")
	public void findInexistentUser() throws UsernameNotFoundException {
		final String username = RandomStringUtils.random(10);
		UsernameNotFoundException thrown = assertThrows(UsernameNotFoundException.class, () -> {
			customUserDetailsService.loadUserByUsername(username);
		});
		
		assertEquals(thrown.getMessage(), "Username: " + username + " not found");
	}
	
	@Test
	@DisplayName("Test if existing user is found by username")
	public void findUserByUsername() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setUsername("cust001");
		customer.setPwd(MockUtils.getPassword(false));
		customerService.save(customer);
		
		User user = (User) customUserDetailsService.loadUserByUsername(customer.getUsername());

		assertEquals(user.getId(), customer.getId());
		assertEquals(user.getUsername(), customer.getUsername());
	}
	
}
