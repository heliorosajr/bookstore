package com.br.store.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.br.store.address.Address;
import com.br.store.book.Book;
import com.br.store.book.service.BookService;
import com.br.store.cart.Cart;
import com.br.store.cart.CartStatus;
import com.br.store.cart.line.CartLine;
import com.br.store.cart.service.CartService;
import com.br.store.city.City;
import com.br.store.city.service.CityService;
import com.br.store.credit.card.CreditCard;
import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.email.address.EmailAddress;
import com.br.store.employee.Employee;
import com.br.store.employee.service.EmployeeService;
import com.br.store.exception.StoreException;
import com.br.store.phone.Phone;
import com.br.store.state.State;
import com.br.store.state.service.StateService;
import com.br.store.user.User;

public class MockUtils {
	
	private static final Logger log = LoggerFactory.getLogger(MockUtils.class);
	
	private static final int MOCK_ITEMS = 300;
	
	private static final BigDecimal MIN_PRICE = BigDecimal.valueOf(49.90);
	private static final BigDecimal MAX_PRICE = BigDecimal.valueOf(299.90);
	
	private static final int MIN_AMOUNT = 5000;
	private static final int MAX_AMOUNT = 10000;
	
	private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	private static String employeePassword;
	private static String customerPassword;
	
	public static void mockEntities(BookService bookService, CustomerService customerService,
			EmployeeService employeeService, CityService cityService, StateService stateService,
			CartService cartService) throws StoreException {
		
		List<Book> books = loadBooks(bookService);
		List<State> states = loadStates(stateService);
		List<City> cities = loadCities(cityService, states);
		List<Customer> customers = loadCustomers(customerService, cities);
		loadEmployees(employeeService, cities);
		loadOrders(cartService, customers, books);
	}
	
	private static List<Book> loadBooks(BookService bookService) throws StoreException {
		List<Book> booksFromDb = bookService.findAll();
		
		if(!booksFromDb.isEmpty()) {
			return booksFromDb;
		}
		
		List<Book> books = new ArrayList<>();
		for(int i = 0; i < MOCK_ITEMS; i++) {
			books.add(createBook(i));
		}
		
		books = bookService.saveAll(books);

		log.info("Books added.");
		return books;
	}
	
	public static Book createBook(int index) {
		Book book = new Book();
		book.setTitle(RandomStringUtils.randomAlphabetic(1, 499));
		book.setDescription(RandomStringUtils.randomAlphabetic(1, 499));
		book.setAuthors(RandomStringUtils.randomAlphabetic(1, 499));
		book.setPageCount(generateRandomInt(80, 500));
		book.setCategories(RandomStringUtils.randomAlphabetic(1, 499));
		book.setIsbn10(index + RandomStringUtils.randomAlphabetic(10-String.valueOf(index).length()));
		book.setIsbn13(index + RandomStringUtils.randomAlphabetic(13-String.valueOf(index).length()));
		book.setQuantity(generateRandomInt(MIN_AMOUNT, MAX_AMOUNT));
		book.setPrice(generateRandomBigDecimal(MIN_PRICE, MAX_PRICE));

		return book;
	}

	public static List<State> loadStates(StateService stateService) throws StoreException {
		List<State> statesFromDb = stateService.findAll();
		
		if(!statesFromDb.isEmpty()) {
			return statesFromDb;
		}
		
		List<State> states = new ArrayList<>();
		
		states.add(createState("AC", "Acre"));
		states.add(createState("AL", "Alagoas"));
		states.add(createState("AP", "Amapá"));
		states.add(createState("AM", "Amazonas"));
		states.add(createState("BA", "Bahia"));
		states.add(createState("CE", "Ceará"));
		states.add(createState("ES", "Espírito Santo"));
		states.add(createState("GO", "Goiás"));
		states.add(createState("MA", "Marannhão"));
		states.add(createState("MT", "Mato Grosso"));
		states.add(createState("MS", "Mato Grosso do Sul"));
		states.add(createState("MG", "Minas Gerais"));
		states.add(createState("PA", "Pará"));
		states.add(createState("PB", "Paraíba"));
		states.add(createState("PR", "Paraná"));
		states.add(createState("PE", "Pernambuco"));
		states.add(createState("PI", "Piauí"));
		states.add(createState("RJ", "Rio de Janeiro"));
		states.add(createState("RN", "Rondônia"));
		states.add(createState("RS", "Rio Grande do Sul"));
		states.add(createState("RO", "Rondônia"));
		states.add(createState("RR", "Roraima"));
		states.add(createState("SC", "Santa Catarina"));
		states.add(createState("SP", "São Paulo"));
		states.add(createState("SE", "Sergipe"));
		states.add(createState("TO", "Tocantins"));
		
		states = stateService.saveAll(states);

		log.info("States added.");
		return states;

	}

	public static List<City> loadCities(CityService cityService, List<State> states) throws StoreException {
		List<City> citiesFromDb = cityService.findAll();
		
		if(!citiesFromDb.isEmpty()) {
			return citiesFromDb;
		}
		
		List<City> cities = new ArrayList<>();
		
		cities.add(createCity("Rio Branco", states.get(0)));
		cities.add(createCity("Maceió", states.get(1)));
		cities.add(createCity("Macapá", states.get(2)));
		cities.add(createCity("Manaus", states.get(3)));
		cities.add(createCity("Salvador", states.get(4)));
		cities.add(createCity("Fortaleza", states.get(5)));
		cities.add(createCity("Vitória", states.get(6)));
		cities.add(createCity("Goiânia", states.get(7)));
		cities.add(createCity("São Luís", states.get(8)));
		cities.add(createCity("Cuiabá", states.get(9)));
		cities.add(createCity("Campo Grande", states.get(10)));
		cities.add(createCity("Belo Horizonte", states.get(11)));
		cities.add(createCity("Belém", states.get(12)));
		cities.add(createCity("João Pessoa", states.get(13)));
		cities.add(createCity("Curitiba", states.get(14)));
		cities.add(createCity("Recife", states.get(15)));
		cities.add(createCity("Teresina", states.get(16)));
		cities.add(createCity("Rio de Janeiro", states.get(17)));
		cities.add(createCity("Natal", states.get(18)));
		cities.add(createCity("Porto Alegre", states.get(19)));
		cities.add(createCity("Porto Velho", states.get(20)));
		cities.add(createCity("Boa Vista", states.get(21)));
		cities.add(createCity("São Paulo", states.get(22)));
		cities.add(createCity("Aracaju", states.get(23)));
		cities.add(createCity("Palmas", states.get(24)));
		
		cities = cityService.saveAll(cities);

		log.info("Cities added.");
		return cities;
	}
	
	public static State createState(String initials, String name) {
		State state = new State();
		state.setInitials(initials);
		state.setName(name);
		
		return state;
	}
	
	private static List<Customer> loadCustomers(CustomerService customerService, List<City> cities) throws StoreException {
		List<Customer> customersFromDb = customerService.findAll();
		
		if(!customersFromDb.isEmpty()) {
			return customersFromDb;
		}
		
		List<Customer> customers = new ArrayList<>();
		for(int i = 0; i < MOCK_ITEMS; i++) {
			customers.add(createCustomer(i, cities));
		}
		
		Customer customer = createCustomer(MOCK_ITEMS + 1, cities);
		customer.setUsername("customer");
		customers.add(customer);
		
		customers = customerService.saveAll(customers);
		log.info("Customers added.");
		
		return customers;
	}
	
	public static Customer createCustomer(int index, List<City> cities) {
		Customer customer = new Customer();
		customer.setFirstName(RandomStringUtils.randomAlphabetic(1, 255));
		customer.setLastName(RandomStringUtils.randomAlphabetic(1, 255));
		customer.setAddress(createAddress(customer, cities));
		customer.setEmails(createEmails());
		customer.setPhoneNumbers(createPhoneNumbers());
		customer.setDateOfBirth(LocalDate.now().minusDays(generateRandomInt(1, 1000)));
		customer.setUsername(index + RandomStringUtils.randomAlphabetic(10-String.valueOf(index).length()));
		customer.setPwd(getPassword(false));
		customer.setRoles(Arrays.asList("ROLE_CUSTOMER"));
		customer.setDateOfRegistration(LocalDate.now().minusDays(generateRandomInt(0, 1000)));
		customer.setCreditCards(createCreditCards(customer));
		
		return customer;
	}
	
	private static Address createAddress(User user, List<City> cities) {
		int cityIndex = generateRandomInt(0, cities.size());
		
		Address address = new Address();
		address.setAddressInformation(RandomStringUtils.randomAlphabetic(1, 499));
		address.setCep(RandomStringUtils.randomNumeric(1, 8));
		address.setCity(cities.get(cityIndex));
		
		return address;
	}
	
	public static City createCity(String name, State state) {
		City city = new City();
		city.setName(name);
		city.setState(state);
		
		return city;
	}
	
	private static List<EmailAddress> createEmails() {
		int emailsTotal = generateRandomInt(1, 3);
		
		List<EmailAddress> emails = new ArrayList<>();
		for(int i = 0; i < emailsTotal; i++) {
			EmailAddress ea = new EmailAddress();
			ea.setEmail(RandomStringUtils.randomAlphabetic(1, 15) + "@" + RandomStringUtils.randomAlphabetic(5) + ".com");
			ea.setDescription(RandomStringUtils.randomAlphabetic(1, 100));
			
			emails.add(ea);
		}
		
		return emails;
	}
	
	private static List<Phone> createPhoneNumbers() {
		int phoneNumbersTotal = generateRandomInt(1, 3);
		
		List<Phone> phoneNumbers = new ArrayList<>();
		for(int i = 0; i < phoneNumbersTotal; i++) {
			Phone phone = new Phone();
			phone.setNumber(RandomStringUtils.randomNumeric(6, 15));
			phone.setDescription(RandomStringUtils.randomAlphabetic(1, 100));
			
			phoneNumbers.add(phone);
		}
		
		return phoneNumbers;
	}
	
	private static List<CreditCard> createCreditCards(Customer customer) {
		int total = generateRandomInt(1, 5);
		
		List<CreditCard> creditCards = new ArrayList<>();
		for(int i = 0; i < total; i++) {
			CreditCard cc = new CreditCard();
			cc.setNumber(RandomStringUtils.randomNumeric(16));
			cc.setName(RandomStringUtils.randomAlphabetic(1, 255));
			cc.setExpiration(LocalDate.now());
			cc.setCustomer(customer);
			
			creditCards.add(cc);
		}
		
		return creditCards;
	}
	
	private static void loadEmployees(EmployeeService employeeService, List<City> cities) throws StoreException {
		if(!employeeService.findAll().isEmpty()) {
			return;
		}
		
		List<Employee> employees = new ArrayList<>();
		for(int i = 0; i < MOCK_ITEMS; i++) {
			employees.add(createEmployee(i, cities));
		}
		Employee admin = createEmployee(MOCK_ITEMS + 1, cities);
		admin.setUsername("adm");
		employees.add(admin);
		
		employeeService.saveAll(employees);
		log.info("Employees added.");
	}
	
	public static Employee createEmployee(int index, List<City> cities) {
		Employee employee = new Employee();
		employee.setFirstName(RandomStringUtils.randomAlphabetic(1, 255));
		employee.setLastName(RandomStringUtils.randomAlphabetic(1, 255));
		employee.setDateOfBirth(LocalDate.now().minusDays(generateRandomInt(1, 1000)));
		employee.setAddress(createAddress(employee, cities));
		employee.setEmails(createEmails());
		employee.setPhoneNumbers(createPhoneNumbers());
		employee.setUsername(index + RandomStringUtils.randomAlphabetic(10-String.valueOf(index).length()));
		employee.setPwd(getPassword(true));
		employee.setRoles(Arrays.asList("ROLE_EMPLOYEE"));
		employee.setEmail(RandomStringUtils.randomAlphabetic(1, 15) + "@" + RandomStringUtils.randomAlphabetic(5) + ".com");
		employee.setDateOfHiring(LocalDate.now().minusDays(generateRandomInt(1, 1000)));
		employee.setDateOfFiring(null);
		employee.setSalary(generateRandomBigDecimal(BigDecimal.valueOf(1500), BigDecimal.valueOf(15000)));
		
		return employee;
	}
	
	private static void loadOrders(CartService cartService, List<Customer> customers, 
			List<Book> books) throws StoreException {
		if(!cartService.findAll().isEmpty()) {
			return;
		}
		
		int bookIndex = 0;
		int customerIndex = 0;
		Customer customer = null;
		List<Cart> cars = new ArrayList<>();
		
		for(int i = 0; i < MOCK_ITEMS; i++) {
			bookIndex = generateRandomInt(0, books.size());
			customerIndex = generateRandomInt(0, customers.size());
			customer = customers.get(customerIndex);
			
			Cart cart = new Cart();
			cart.setCustomer(customer);
			cart.setCreditCard(customer.getCreditCards().get(0));
			cart.setCartLines(createLines(cart, books.get(bookIndex)));
			cart.setStatus(CartStatus.PENDING);
			
			cars.add(cartService.save(cart));
		}
		
		log.info("Carts added.");
	}
	
	private static List<CartLine> createLines(Cart cart, Book book) {
		List<CartLine> cartLines = new ArrayList<>();
		int lines = generateRandomInt(1, 5);
		
		for(int i = 0; i < lines; i++) {
			CartLine cl = new CartLine();
			cl.setBook(book);
			cl.setNumberOfItems(generateRandomInt(1, 3));
			cl.setCart(cart);
			
			cartLines.add(cl);
		}
		
		return cartLines;
	}
	
	private static BigDecimal generateRandomBigDecimal(BigDecimal min, BigDecimal max) {
	    BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
	   
	    return randomBigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	private static int generateRandomInt(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}
	
	public static String getPassword(boolean employee) {
		if(employee) {
			employeePassword = employeePassword == null ? passwordEncoder.encode("empPwd") : employeePassword;
			return employeePassword;
		}
		
		customerPassword = customerPassword == null ? passwordEncoder.encode("custPwd") : customerPassword;
		return customerPassword;
	}
	
}
