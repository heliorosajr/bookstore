package com.br.store.utils;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.br.store.city.City;
import com.br.store.city.service.CityService;
import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.employee.Employee;
import com.br.store.employee.service.EmployeeService;
import com.br.store.error.CustomError;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.State;
import com.br.store.state.service.StateService;
import com.br.store.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class TestUtils {
	
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	
	public static void assertStoreException(StoreException thrown,
			String message, String details, ErrorDomain errorDomain, ErrorCode errorCode) {
		
		assertEquals(message, thrown.getMessage());
		assertEquals(details, thrown.getDetails());
		assertEquals(errorDomain, thrown.getErrorDomain());
		assertEquals(errorCode, thrown.getErrorCode());
	}
	
	public static void assertCustomError(CustomError error,
			String message, String details, ErrorDomain errorDomain, ErrorCode errorCode) {
		
		assertEquals(error.getErrorMessage(), message);
		assertEquals(error.getErrorDetails(), details);
		assertEquals(error.getErrorCode(), errorCode);
		assertEquals(error.getErrorDomain(), errorDomain);
	}
	
	public static MediaType getJsonMediaType() {
		return new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	}
	
	public static String getToken(MockMvc mockMvc, String username, String password) throws StoreException {
		JsonObject requestJson = new JsonObject();
		requestJson.addProperty("username", username);
		requestJson.addProperty("password", password);
		
		MvcResult result;
		try {
			result = mockMvc.perform(post("/auth/signin")
				.contentType(TestUtils.getJsonMediaType())
			    .content(requestJson.toString()))
				.andDo(print())
			    .andReturn();
			JsonObject json = JsonUtils.parseToJson(result.getResponse().getContentAsString());
			
			if(result.getResolvedException() != null) {
				throw result.getResolvedException();
			}
			
			return JsonUtils.getJsonPropertyAsString(json, "token");
		} catch (StoreException e) {
			throw e;
		} catch (Exception e) {
			throw new StoreException(ErrorDomain.AUTHENTICATION, ErrorCode.SERVER_ERROR, "Could not authenticate user", e);
		}
	}
	
	public static User getUser(StateService stateService, CityService cityService, boolean employee, int index) throws StoreException {
		List<State> states = MockUtils.loadStates(stateService);
		List<City> cities = MockUtils.loadCities(cityService, states);
		
		if(employee) {
			return MockUtils.createEmployee(index, cities);
		}
		
		return MockUtils.createCustomer(index, cities);
	}
	
	public static Employee getEmployee(EmployeeService employeeService, StateService stateService, CityService cityService, int index) throws StoreException {
		Employee emp = (Employee) getUser(stateService, cityService, true, index);
		emp.setUsername("employee" + index);
		emp.setPwd(MockUtils.getPassword(true));
		
		return employeeService.save(emp);
	}
	
	public static Customer getCustomer(CustomerService customerService, StateService stateService, CityService cityService, int index) throws StoreException {
		Customer cust = (Customer) getUser(stateService, cityService, false, index);
		cust.setUsername("customer" + index);
		cust.setPwd(MockUtils.getPassword(false));
		
		return customerService.save(cust);
	}
	
	public static Gson getGson() {
		return new GsonBuilder().registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>() {
            @Override
            public LocalDate deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return LocalDate.parse(json.getAsJsonPrimitive().getAsString());
            }
        }).create();
	}
	
	public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.setSerializationInclusion(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
	
}
