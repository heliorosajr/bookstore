package com.br.store.authentication.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.customer.service.CustomerService;
import com.br.store.employee.service.EmployeeService;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.service.StateService;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = StoreApplication.class, properties = {"spring.config.name=authenticationDb","store.trx.datasource.url=jdbc:h2:mem:authenticationDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class AuthenticationControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private EmployeeService employeeService;

	// ----------------------------------------------------
	// Customer endpoints authentication
	// ----------------------------------------------------
	@Test
	@DisplayName("Test if /customer/find-all endpoint requires authentication")
	public void assertCustomerFindAllRequiresAuthentication() throws Exception {
		mockMvc.perform(get("/customer/find-all"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /customer/find/{id} endpoint requires authentication")
	public void assertCustomerFindOneRequiresAuthentication() throws Exception {
		mockMvc.perform(get("/customer/find/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /customer/create endpoint requires authentication")
	public void assertCustomerCreateRequiresAuthentication() throws Exception {
		mockMvc.perform(post("/customer/create"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /customer/delete/{id} endpoint requires authentication")
	public void assertCustomerDeleteRequiresAuthentication() throws Exception {
		mockMvc.perform(delete("/customer/delete/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /customer/update/{id} endpoint requires authentication")
	public void assertCustomerUpdateRequiresAuthentication() throws Exception {
		mockMvc.perform(put("/customer/update/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	// ----------------------------------------------------
	// Employee endpoints authentication
	// ----------------------------------------------------
	@Test
	@DisplayName("Test if /employee/find-all endpoint requires authentication")
	public void assertEmployeeFindAllRequiresAuthentication() throws Exception {
		mockMvc.perform(get("/employee/find-all"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /employee/find/{id} endpoint requires authentication")
	public void assertEmployeeFindOneRequiresAuthentication() throws Exception {
		mockMvc.perform(get("/employee/find/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /employee/create endpoint requires authentication")
	public void assertEmployeeCreateRequiresAuthentication() throws Exception {
		mockMvc.perform(post("/employee/create"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /employee/delete/{id} endpoint requires authentication")
	public void assertEmployeeDeleteRequiresAuthentication() throws Exception {
		mockMvc.perform(delete("/employee/delete/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /employee/update/{id} endpoint requires authentication")
	public void assertEmployeeUpdateRequiresAuthentication() throws Exception {
		mockMvc.perform(put("/employee/update/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	// ----------------------------------------------------
	// Book endpoints authentication
	// ----------------------------------------------------
	@Test
	@DisplayName("Test if /book/create endpoint requires authentication")
	public void assertBookCreateRequiresAuthentication() throws Exception {
		mockMvc.perform(post("/book/create"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /book/delete/{id} endpoint requires authentication")
	public void assertBookDeleteRequiresAuthentication() throws Exception {
		mockMvc.perform(delete("/book/delete/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /book/update/{id} endpoint requires authentication")
	public void assertBookUpdateRequiresAuthentication() throws Exception {
		mockMvc.perform(put("/book/update/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /book/increase-stock/{id} endpoint requires authentication")
	public void assertBookIncreaseStockRequiresAuthentication() throws Exception {
		mockMvc.perform(put("/book/increase-stock/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	@Test
	@DisplayName("Test if /book/remove-from-stock/{id} endpoint requires authentication")
	public void assertBookRemoveFromStockRequiresAuthentication() throws Exception {
		mockMvc.perform(put("/book/remove-from-stock/1"))
        .andDo(print())
        .andExpect(status().isForbidden());
	}
	
	// ----------------------------------------------------
	// Test authentication
	// ----------------------------------------------------
	@Test
	@DisplayName("Test if authentication token is created for customer")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void assertTokenIsCreatedForCustomer() throws Exception {
		TestUtils.getCustomer(customerService, stateService, cityService, 1);
		
		assertNotNull(TestUtils.getToken(mockMvc, "customer1", "custPwd"));
	}
	
	@Test
	@DisplayName("Test if authentication token is created for employee")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void assertTokenIsCreatedForEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		
		assertNotNull(TestUtils.getToken(mockMvc, "employee1", "empPwd"));
	}
	
	@Test
	@DisplayName("Test if authentication token is not created for customer with wrong password")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void assertTokenIsNotCreatedForCustomerWithWrongPwd() throws Exception {
		TestUtils.getCustomer(customerService, stateService, cityService, 1);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			TestUtils.getToken(mockMvc, "customer1", "asd");
		});
		
		TestUtils.assertStoreException(thrown, "Invalid username/password supplied",
				"Bad credentials", ErrorDomain.AUTHENTICATION, ErrorCode.AUTHENTICATION_FAILED);
		
	}
	
}