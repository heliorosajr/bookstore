package com.br.store;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StoreApplicationTests {

	@Test
	void contextLoads() {
		StoreApplication.main(new String[] {});
		assertTrue(true);
	}

}
