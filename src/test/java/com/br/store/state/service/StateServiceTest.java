package com.br.store.state.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.store.StoreApplication;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.State;
import com.br.store.utils.Constants;
import com.br.store.utils.MockUtils;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=stateDb","store.trx.datasource.url=jdbc:h2:mem:stateDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class StateServiceTest {

	@Autowired
	private StateService stateService;

	@Test
	@DisplayName("Test if exception is thrown when a state is not found")
	public void findInexistentEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.findOne(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "state", Long.MAX_VALUE),
			ErrorDomain.STATE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while updating an inexistent state")
	public void updateInexistentEntity() throws StoreException {
		State entity = MockUtils.createState("SP", "São Paulo");
		entity.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.update(entity, Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "state", Long.MAX_VALUE),
			ErrorDomain.STATE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown when null id is given")
	public void findNullEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.findOne(null);
		});

		TestUtils.assertStoreException(thrown, "Error retrieving state with id null",
			"The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!",
			ErrorDomain.STATE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while deleting an inexistent state")
	public void deleteInexistentEntity() throws StoreException {
		State entity = MockUtils.createState("SP", "São Paulo");
		entity.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.delete(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "state", Long.MAX_VALUE),
			ErrorDomain.STATE, ErrorCode.ENTITY_NOT_FOUND);
	}

	@Test
	@DisplayName("Test generic exception when saving state")
	public void persistNullState() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save state",
			thrown.getDetails(), ErrorDomain.STATE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test generic exception when saving state list")
	public void persistNullStateList() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.saveAll(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save states",
			thrown.getDetails(), ErrorDomain.STATE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if a state without initials is persisted")
	public void persistStateWithoutInitials() throws StoreException {
		State entity = MockUtils.createState(null, "São Paulo");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "State initials"), ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state with empty initials is persisted")
	public void persistStateWithEmptyInitials() throws StoreException {
		State entity = MockUtils.createState("", "São Paulo");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "State initials"), ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}

	@Test
	@DisplayName("Test if a state with one initial is persisted")
	public void persistStateWithOneInitial() throws StoreException {
		State entity = MockUtils.createState("A", "São Paulo");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			Constants.STATE_INITIALS_LENGTH, ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state with initials that exceeds limit is persisted")
	public void persistStateWithInitialsExceedingLimit() throws StoreException {
		State entity = MockUtils.createState("AAA", "São Paulo");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			Constants.STATE_INITIALS_LENGTH, ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state without name is persisted")
	public void persistStateWithoutName() throws StoreException {
		State entity = MockUtils.createState("SP", null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "State name"), ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state with empty name is persisted")
	public void persistStateWithEmptyName() throws StoreException {
		State entity = MockUtils.createState("AA", "");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "State name"), ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state with name that exceeds limit is persisted")
	public void persistStateWithNameExceedingLimit() throws StoreException {
		State entity = MockUtils.createState("SP", RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "State name", 255), ErrorDomain.STATE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a state is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistState() throws StoreException {
		State entity = stateService.save(MockUtils.createState("SP", "São Paulo"));
		State entityFromDb = stateService.findOne(entity.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getInitials(), entity.getInitials());
	}
	
	@Test
	@DisplayName("Test if an existing state is not persisted twice")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistExistingState() throws StoreException {
		State entity = stateService.save(MockUtils.createState("XY", RandomStringUtils.randomAlphabetic(240)));
		State entityFromDb = stateService.findOne(entity.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getInitials(), entity.getInitials());
		
		entity = stateService.save(entity);
		entityFromDb = stateService.findOne(entity.getId());
		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getInitials(), entity.getInitials());
	}
	
	@Test
	@DisplayName("Test if a list of states is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void findAll() throws StoreException {
		State sp = MockUtils.createState("SP", "São Paulo");
		State rj = MockUtils.createState("RJ", "Rio de Janeiro");
		State mg = MockUtils.createState("MG", "Minas Gerais");
		
		List<State> entities = new ArrayList<>();
		entities.add(sp);
		entities.add(rj);
		entities.add(mg);
		entities = stateService.saveAll(entities);
		
		List<State> entitiesFromDb = stateService.findAll();
		assertEquals(entities.size(), entitiesFromDb.size());
		assertEquals(entities, entitiesFromDb);
	}
	
	@Test
	@DisplayName("Test generic exception when updating state")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void updateNullState() throws StoreException {
		State entity = stateService.save(MockUtils.createState("SP", "São Paulo"));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.update(null, entity.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not update state with id " + entity.getId(),
			thrown.getDetails(), ErrorDomain.STATE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if an state is updated")
	public void update() throws StoreException {
		State entity = stateService.save(MockUtils.createState("FF", "Teste"));
		
		assertNotNull(entity.getId());
		assertEquals(entity.getInitials(), "FF");
		assertEquals(entity.getName(), "Teste");
		
		entity.setInitials("AA");
		entity.setName("Updated");
		stateService.update(entity, entity.getId());
		
		State entityFromDb = stateService.findOne(entity.getId());
		
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getInitials(), "AA");
		assertEquals(entityFromDb.getName(), "Updated");
	}
	
	@Test
	@DisplayName("Test if an state is not updated if it breaks unique constraint")
	public void updateFail() throws StoreException {
		State targetEntity = stateService.save(MockUtils.createState("SP", "São Paulo"));
		final State entityToUpdate = stateService.save(MockUtils.createState("aa", "sp"));
		
		assertNotNull(targetEntity.getId());
		assertNotNull(entityToUpdate.getId());
		
		entityToUpdate.setInitials("SP");
		entityToUpdate.setName("São Paulo");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.update(entityToUpdate, entityToUpdate.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not update state with id " + entityToUpdate.getId(),
			"State already exists", ErrorDomain.STATE, ErrorCode.SERVER_ERROR);
	}

	@Test
	@DisplayName("Test if a state can be deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void deleteState() throws StoreException {
		State entity = stateService.save(MockUtils.createState("FF", "Teste"));
		
		assertNotNull(entity.getId());
		assertEquals(entity.getInitials(), "FF");
		assertEquals(entity.getName(), "Teste");
		
		stateService.delete(entity.getId());
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			stateService.findOne(entity.getId());
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "state", entity.getId()),
			ErrorDomain.STATE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
}
