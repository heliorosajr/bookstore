package com.br.store.employee.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.employee.Employee;
import com.br.store.employee.service.EmployeeService;
import com.br.store.error.CustomError;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.JsonUtils;
import com.br.store.utils.TestUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonObject;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = StoreApplication.class, properties = {"spring.config.name=empControllerDb","store.trx.datasource.url=jdbc:h2:mem:empControllerDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class EmployeeControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Test
	@DisplayName("Test if employee is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findExistingEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Employee employee = TestUtils.getEmployee(employeeService, stateService, cityService, 2);

		MvcResult result = mockMvc.perform(get("/employee/find/" + employee.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
		Employee employeeResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Employee.class);
       
        assertEquals(employee.getId(), employeeResponse.getId());
        assertEquals(employee.getFirstName(), employeeResponse.getFirstName());
		assertEquals(employee.getLastName(), employeeResponse.getLastName());
		assertEquals(employee.getSalary(), employeeResponse.getSalary());
    }
	
	@Test
	@DisplayName("Test if exception is thrown when finding inexistent employee")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findInexistentEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);

		MvcResult result = mockMvc.perform(get("/employee/find/" + Long.MAX_VALUE)
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
        CustomError error = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), CustomError.class);
        
		TestUtils.assertCustomError(error, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", Long.MAX_VALUE),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
    }
	
	@Test
	@DisplayName("Test if employee list is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findAll() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Employee employee1 = TestUtils.getEmployee(employeeService, stateService, cityService, 2);
		Employee employee2 = TestUtils.getEmployee(employeeService, stateService, cityService, 3);
		Employee employee3 = TestUtils.getEmployee(employeeService, stateService, cityService, 4);
		
		List<Employee> entities = new ArrayList<>();
		entities.add(employee1);
		entities.add(employee2);
		entities.add(employee3);
		entities = employeeService.saveAll(entities);
		
		MvcResult result = mockMvc.perform(get("/employee/find-all")
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
		JsonObject json = JsonUtils.parseToJson(result.getResponse().getContentAsString());
		com.google.gson.JsonArray content = json.getAsJsonArray("content");
		@SuppressWarnings("serial")
		List<Employee> emplloyeesResponse = TestUtils.getGson().fromJson(content.toString(), new TypeToken<List<Employee>>(){}.getType());
        
        List<Employee> entitiesFromDb = employeeService.findAll();
		assertEquals(entitiesFromDb.size(), emplloyeesResponse.size());
		for(int i = 0; i < entitiesFromDb.size(); i++) {
			assertEquals(entitiesFromDb.get(i).getId(), emplloyeesResponse.get(i).getId());
			assertEquals(entitiesFromDb.get(i).getFirstName(), emplloyeesResponse.get(i).getFirstName());
			assertEquals(entitiesFromDb.get(i).getLastName(), emplloyeesResponse.get(i).getLastName());
			assertEquals(entitiesFromDb.get(i).getSalary(), emplloyeesResponse.get(i).getSalary());
		}
    }
	
	@Test
	@DisplayName("Test if employee is created")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void createEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 2);
		employee.setFirstName("Creating");
		
		MvcResult result = mockMvc.perform(post("/employee/create")
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd"))
			.contentType(TestUtils.APPLICATION_JSON_UTF8)
			.content(TestUtils.convertObjectToJsonBytes(employee)))
			.andDo(print())
			.andReturn();
		
		Employee employeeResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Employee.class);
	       
        assertNotNull(employeeResponse.getId());
        assertNotNull(employeeResponse.getSalary());
        assertEquals(employeeResponse.getFirstName(), "Creating");
    }
	
	@Test
	@DisplayName("Test if employee is deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void deleteCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Employee employee = TestUtils.getEmployee(employeeService, stateService, cityService, 2);

		mockMvc.perform(delete("/employee/delete/" + employee.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andExpect(status().isOk());
    }
	
	@Test
	@DisplayName("Test if exception is thrown when deleting inexistent employee")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void deleteInexistentEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);

		MvcResult result = mockMvc.perform(delete("/employee/delete/" + Long.MAX_VALUE)
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
        CustomError error = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), CustomError.class);
        
		TestUtils.assertCustomError(error, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", Long.MAX_VALUE),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
    }
	
	@Test
	@DisplayName("Test if employee is updated")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void updateEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Employee employee = TestUtils.getEmployee(employeeService, stateService, cityService, 2);
		final String nameBefore = employee.getFirstName();
		employee.setFirstName("updated");
		
		MvcResult result = mockMvc.perform(put("/employee/update/" + employee.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd"))
			.contentType(TestUtils.APPLICATION_JSON_UTF8)
			.content(TestUtils.convertObjectToJsonBytes(employee)))
			.andDo(print())
			.andReturn();
		
		Employee employeeResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Employee.class);
	       
        assertEquals(employee.getId(), employeeResponse.getId());
        assertNotEquals(nameBefore, employeeResponse.getFirstName());
		assertEquals(employeeResponse.getFirstName(), "updated");
    }

}
