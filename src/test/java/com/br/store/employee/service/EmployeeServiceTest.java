package com.br.store.employee.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.UnexpectedRollbackException;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.employee.Employee;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=employeeDb","store.trx.datasource.url=jdbc:h2:mem:employeeDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class EmployeeServiceTest {
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Test
	@DisplayName("Test if an employee without email is persisted")
	public void persistEmployeeWithoutEmail() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setEmail(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee email address"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee with empty email is persisted")
	public void persistEmployeeWithEmptyEmail() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setEmail(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee email address"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee with email that exceeds limit is persisted")
	public void persistEmployeeEmailExceedingLimit() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setEmail(RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Employee email address", 255), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employeed with invalid contact email is persisted")
	public void persistEmployeeWithInvalidEmail() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setEmail("email");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMAIL_ERROR, "email"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee without date of hiring is persisted")
	public void persistEmployeeWithoutDateOfHiring() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setDateOfHiring(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee date of hiring"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee without salary is persisted")
	public void persistEmployeeWithoutSalary() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setSalary(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Employee salary"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an employee with negative salary is persisted")
	public void persistEmployeeWithNegativeSalary() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setSalary(BigDecimal.valueOf(-10.50));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(employee);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.NEGATIVE_VALUE, "Employee salary"), ErrorDomain.EMPLOYEE, ErrorCode.BAD_REQUEST);
	}

	@Test
	@DisplayName("Test if exception is thrown when an employee is not found")
	public void findInexistentEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.findOne(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", Long.MAX_VALUE),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown when null id is given")
	public void findNullEntity() throws UnexpectedRollbackException {
		UnexpectedRollbackException thrown = assertThrows(UnexpectedRollbackException.class, () -> {
			employeeService.findOne(null);
		});

		assertEquals(thrown.getMessage(), "Transaction silently rolled back because it has been marked as rollback-only");
	}
	
	@Test
	@DisplayName("Test if exception is thrown while updating an inexistent employee")
	public void updateInexistentEntity() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.update(employee, Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", Long.MAX_VALUE),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while deleting an inexistent employee")
	public void deleteInexistentEntity() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.delete(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", Long.MAX_VALUE),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test generic exception when saving employee")
	public void persistNullEmployee() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.save(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save employee",
			thrown.getDetails(), ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if an employee is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistEmployee() throws StoreException {
		Employee employee = employeeService.save((Employee) TestUtils.getUser(stateService, cityService, true, 1));
		Employee entityFromDb = employeeService.findOne(employee.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), employee.getId());
		assertEquals(entityFromDb.getFirstName(), employee.getFirstName());
		assertEquals(entityFromDb.getLastName(), employee.getLastName());
		assertEquals(entityFromDb.getEmail(), employee.getEmail());
		assertEquals(entityFromDb.getSalary(), employee.getSalary());
	}
	
	@Test
	@DisplayName("Test if a list of employees is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void findAll() throws StoreException {
		Employee employee1 = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		Employee employee2 = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		Employee employee3 = (Employee) TestUtils.getUser(stateService, cityService, true, 1);

		List<Employee> entities = new ArrayList<>();
		entities.add(employee1);
		entities.add(employee2);
		entities.add(employee3);
		entities = employeeService.saveAll(entities);
		
		List<Employee> entitiesFromDb = employeeService.findAll();
		assertEquals(entities.size(), entitiesFromDb.size());
		for(int i = 0; i < entities.size(); i++) {
			assertEquals(entitiesFromDb.get(i).getId(), entities.get(i).getId());
			assertEquals(entitiesFromDb.get(i).getFirstName(), entities.get(i).getFirstName());
			assertEquals(entitiesFromDb.get(i).getLastName(), entities.get(i).getLastName());
			assertEquals(entitiesFromDb.get(i).getEmail(), entities.get(i).getEmail());
			assertEquals(entitiesFromDb.get(i).getSalary(), entities.get(i).getSalary());
		}
	}
	
	@Test
	@DisplayName("Test generic exception when updating employee")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void updateNullEmployee() throws StoreException {
		Employee entity = employeeService.save((Employee) TestUtils.getUser(stateService, cityService, true, 1));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.update(null, entity.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not save employee",
			thrown.getDetails(), ErrorDomain.EMPLOYEE, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if an employee is updated")
	public void update() throws StoreException {
		Employee employee = (Employee) TestUtils.getUser(stateService, cityService, true, 1);
		employee.setFirstName("Helio");
		employee = employeeService.save(employee);
		Employee entityFromDb = employeeService.findOne(employee.getId());
		
		assertNotNull(entityFromDb.getId());
		assertEquals(entityFromDb.getFirstName(), "Helio");
		
		employee.setFirstName("Updated");
		entityFromDb = employeeService.update(employee, employee.getId());
		
		assertEquals(entityFromDb.getId(), employee.getId());
		assertEquals(entityFromDb.getFirstName(), "Updated");
	}
	
	@Test
	@DisplayName("Test if an employee can be deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void deleteEmployee() throws StoreException {
		Employee employee = employeeService.save((Employee) TestUtils.getUser(stateService, cityService, true, 1));
		Employee entityFromDb = employeeService.findOne(employee.getId());
		
		assertEquals(entityFromDb.getId(), employee.getId());
		assertEquals(entityFromDb.getFirstName(), employee.getFirstName());
		assertEquals(entityFromDb.getLastName(), employee.getLastName());
		assertEquals(entityFromDb.getEmail(), employee.getEmail());
		assertEquals(entityFromDb.getSalary(), employee.getSalary());
		
		employeeService.delete(employee.getId());
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			employeeService.findOne(employee.getId());
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "employee", employee.getId()),
			ErrorDomain.EMPLOYEE, ErrorCode.ENTITY_NOT_FOUND);
	}
	
}
