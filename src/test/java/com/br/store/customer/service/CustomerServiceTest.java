package com.br.store.customer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.customer.Customer;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=customerDb","store.trx.datasource.url=jdbc:h2:mem:customerDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CustomerServiceTest {
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private CustomerService customerService;
	
	@Test
	@DisplayName("Test if an customer without date of registration is persisted")
	public void persistCustomerWithoutDateOfRegistrationl() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setDateOfRegistration(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer date of registration"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if an customer with invalid date of registration is persisted")
	public void persistCustomerWithInvalidDateOfRegistration() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setDateOfRegistration(LocalDate.now().plusDays(100));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.DATE_ERROR, "Customer date of registration", "future"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card without number is persisted")
	public void persistCustomerCreditCardWithoutNumber() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setNumber(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer credit card number"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card with empty number is persisted")
	public void persistCustomerCreditCardWithEmptyNumber() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setNumber("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer credit card number"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card number that exceeds limit is persisted")
	public void persistCustomerCreditCardWithNumberExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setNumber(RandomStringUtils.randomAlphabetic(50));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Customer credit card number", 16), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card without name is persisted")
	public void persistCustomerCreditCardWithoutName() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setName(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer credit card name"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card with empty name is persisted")
	public void persistCustomerCreditCardWithEmptyName() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setName("");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer credit card name"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer credit card name that exceeds limit is persisted")
	public void persistCustomerCreditCardWithNameExceedingLimit() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setName(RandomStringUtils.randomAlphabetic(300));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "Customer credit card name", 255), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a customer with credit card without expiration is persisted")
	public void persistCustomerCreditCardWithoutExpiration() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.getCreditCards().get(0).setExpiration(null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(customer);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "Customer credit card expiration"), ErrorDomain.CUSTOMER, ErrorCode.BAD_REQUEST);
	}

	@Test
	@DisplayName("Test if exception is thrown when a customer is not found")
	public void findInexistentEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.findOne(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", Long.MAX_VALUE),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown when null id is given")
	public void findNullEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.findOne(null);
		});

		TestUtils.assertStoreException(thrown, "Error retrieving customer with id null",
			"The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!",
			ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while updating an inexistent customer")
	public void updateInexistentEntity() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.update(customer, Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", Long.MAX_VALUE),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while deleting an inexistent customer")
	public void deleteInexistentEntity() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.delete(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", Long.MAX_VALUE),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test generic exception when saving customer")
	public void persistNullCustomer() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.save(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save customer",
			thrown.getDetails(), ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if an customer is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistCustomer() throws StoreException {
		Customer customer = customerService.save((Customer) TestUtils.getUser(stateService, cityService, false, 1));
		Customer entityFromDb = customerService.findOne(customer.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), customer.getId());
		assertEquals(entityFromDb.getFirstName(), customer.getFirstName());
		assertEquals(entityFromDb.getLastName(), customer.getLastName());
		assertEquals(entityFromDb.getDateOfRegistration(), customer.getDateOfRegistration());
	}
	
	@Test
	@DisplayName("Test if a list of customers is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void findAll() throws StoreException {
		Customer customer1 = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		Customer customer2 = (Customer) TestUtils.getUser(stateService, cityService, false, 2);
		Customer customer3 = (Customer) TestUtils.getUser(stateService, cityService, false, 3);

		List<Customer> entities = new ArrayList<>();
		entities.add(customer1);
		entities.add(customer2);
		entities.add(customer3);
		entities = customerService.saveAll(entities);
		
		List<Customer> entitiesFromDb = customerService.findAll();
		assertEquals(entities.size(), entitiesFromDb.size());
		for(int i = 0; i < entities.size(); i++) {
			assertEquals(entitiesFromDb.get(i).getId(), entities.get(i).getId());
			assertEquals(entitiesFromDb.get(i).getFirstName(), entities.get(i).getFirstName());
			assertEquals(entitiesFromDb.get(i).getLastName(), entities.get(i).getLastName());
			assertEquals(entitiesFromDb.get(i).getDateOfRegistration(), entities.get(i).getDateOfRegistration());
		}
	}
	
	@Test
	@DisplayName("Test generic exception when updating customer")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void updateNullCustomer() throws StoreException {
		Customer entity = customerService.save((Customer) TestUtils.getUser(stateService, cityService, false, 1));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.update(null, entity.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not save customer",
			thrown.getDetails(), ErrorDomain.CUSTOMER, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if a customer is updated")
	public void update() throws StoreException {
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setFirstName("Helio");
		customer = customerService.save(customer);
		Customer entityFromDb = customerService.findOne(customer.getId());
		
		assertNotNull(entityFromDb.getId());
		assertEquals(entityFromDb.getFirstName(), "Helio");
		
		customer.setFirstName("Updated");
		entityFromDb = customerService.update(customer, customer.getId());
		
		assertEquals(entityFromDb.getId(), customer.getId());
		assertEquals(entityFromDb.getFirstName(), "Updated");
	}
	
	@Test
	@DisplayName("Test if a customer can be deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void deleteCustomer() throws StoreException {
		Customer customer = customerService.save((Customer) TestUtils.getUser(stateService, cityService, false, 1));
		Customer entityFromDb = customerService.findOne(customer.getId());
		
		assertEquals(entityFromDb.getId(), customer.getId());
		assertEquals(entityFromDb.getFirstName(), customer.getFirstName());
		assertEquals(entityFromDb.getLastName(), customer.getLastName());
		assertEquals(entityFromDb.getDateOfRegistration(), customer.getDateOfRegistration());
		
		customerService.delete(customer.getId());
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			customerService.findOne(customer.getId());
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", customer.getId()),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
	}
	
}
