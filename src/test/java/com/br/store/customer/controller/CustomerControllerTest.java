package com.br.store.customer.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.br.store.StoreApplication;
import com.br.store.city.service.CityService;
import com.br.store.customer.Customer;
import com.br.store.customer.service.CustomerService;
import com.br.store.employee.service.EmployeeService;
import com.br.store.error.CustomError;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.JsonUtils;
import com.br.store.utils.TestUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonObject;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = StoreApplication.class, properties = {"spring.config.name=custControllerDb","store.trx.datasource.url=jdbc:h2:mem:custControllerDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CustomerControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Test
	@DisplayName("Test if customer is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findExistingCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Customer customer = TestUtils.getCustomer(customerService, stateService, cityService, 1);

		MvcResult result = mockMvc.perform(get("/customer/find/" + customer.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
        Customer customerResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Customer.class);
       
        assertEquals(customer.getId(), customerResponse.getId());
        assertEquals(customer.getFirstName(), customerResponse.getFirstName());
		assertEquals(customer.getLastName(), customerResponse.getLastName());
		assertEquals(customer.getDateOfRegistration(), customerResponse.getDateOfRegistration());
    }
	
	@Test
	@DisplayName("Test if exception is thrown when finding inexistent customer")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findInexistentCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);

		MvcResult result = mockMvc.perform(get("/customer/find/" + Long.MAX_VALUE)
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
        CustomError error = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), CustomError.class);
        
		TestUtils.assertCustomError(error, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", Long.MAX_VALUE),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
    }
	
	@Test
	@DisplayName("Test if customer list is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void findAll() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Customer customer1 = TestUtils.getCustomer(customerService, stateService, cityService, 1);
		Customer customer2 = TestUtils.getCustomer(customerService, stateService, cityService, 2);
		Customer customer3 = TestUtils.getCustomer(customerService, stateService, cityService, 3);
		
		List<Customer> entities = new ArrayList<>();
		entities.add(customer1);
		entities.add(customer2);
		entities.add(customer3);
		entities = customerService.saveAll(entities);
		
		MvcResult result = mockMvc.perform(get("/customer/find-all")
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
		JsonObject json = JsonUtils.parseToJson(result.getResponse().getContentAsString());
		com.google.gson.JsonArray content = json.getAsJsonArray("content");
		@SuppressWarnings("serial")
		List<Customer> customersResponse = TestUtils.getGson().fromJson(content.toString(), new TypeToken<List<Customer>>(){}.getType());
        
        List<Customer> entitiesFromDb = customerService.findAll();
		assertEquals(entitiesFromDb.size(), customersResponse.size());
		for(int i = 0; i < entitiesFromDb.size(); i++) {
			assertEquals(entitiesFromDb.get(i).getId(), customersResponse.get(i).getId());
			assertEquals(entitiesFromDb.get(i).getFirstName(), customersResponse.get(i).getFirstName());
			assertEquals(entitiesFromDb.get(i).getLastName(), customersResponse.get(i).getLastName());
			assertEquals(entitiesFromDb.get(i).getDateOfRegistration(), customersResponse.get(i).getDateOfRegistration());
		}
    }
	
	@Test
	@DisplayName("Test if customer is created")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void createCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Customer customer = (Customer) TestUtils.getUser(stateService, cityService, false, 1);
		customer.setFirstName("Creating");
		
		MvcResult result = mockMvc.perform(post("/customer/create")
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd"))
			.contentType(TestUtils.APPLICATION_JSON_UTF8)
			.content(TestUtils.convertObjectToJsonBytes(customer)))
			.andDo(print())
			.andReturn();
		
		Customer customerResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Customer.class);
	       
        assertNotNull(customerResponse.getId());
        assertEquals(customerResponse.getFirstName(), "Creating");
    }
	
	@Test
	@DisplayName("Test if customer is deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void deleteCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Customer customer = TestUtils.getCustomer(customerService, stateService, cityService, 1);

		mockMvc.perform(delete("/customer/delete/" + customer.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andExpect(status().isOk());
    }
	
	@Test
	@DisplayName("Test if exception is thrown when deleting inexistent customer")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void deleteInexistentEmployee() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);

		MvcResult result = mockMvc.perform(delete("/customer/delete/" + Long.MAX_VALUE)
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd")))
			.andDo(print())
			.andReturn();
		
        CustomError error = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), CustomError.class);
        
		TestUtils.assertCustomError(error, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "customer", Long.MAX_VALUE),
			ErrorDomain.CUSTOMER, ErrorCode.ENTITY_NOT_FOUND);
    }
	
	@Test
	@DisplayName("Test if customer is updated")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
    public void updateCustomer() throws Exception {
		TestUtils.getEmployee(employeeService, stateService, cityService, 1);
		Customer customer = TestUtils.getCustomer(customerService, stateService, cityService, 1);
		final String nameBefore = customer.getFirstName();
		customer.setFirstName("updated");
		
		MvcResult result = mockMvc.perform(put("/customer/update/" + customer.getId())
			.header("Authorization", "Bearer " + TestUtils.getToken(mockMvc, "employee1", "empPwd"))
			.contentType(TestUtils.APPLICATION_JSON_UTF8)
			.content(TestUtils.convertObjectToJsonBytes(customer)))
			.andDo(print())
			.andReturn();
		
		Customer customerResponse = TestUtils.getGson().fromJson(result.getResponse().getContentAsString(), Customer.class);
	       
        assertEquals(customer.getId(), customerResponse.getId());
        assertNotEquals(nameBefore, customerResponse.getFirstName());
		assertEquals(customerResponse.getFirstName(), "updated");
    }
	
}
