package com.br.store.city.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.store.StoreApplication;
import com.br.store.city.City;
import com.br.store.error.ErrorCode;
import com.br.store.error.ErrorDomain;
import com.br.store.exception.StoreException;
import com.br.store.state.State;
import com.br.store.state.service.StateService;
import com.br.store.utils.Constants;
import com.br.store.utils.MockUtils;
import com.br.store.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApplication.class, properties = {"spring.config.name=cityDb","store.trx.datasource.url=jdbc:h2:mem:cityDb"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CityServiceTest {

	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Test
	@DisplayName("Test if exception is thrown when a city is not found")
	public void findInexistentEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.findOne(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "city", Long.MAX_VALUE),
			ErrorDomain.CITY, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown when null id is given")
	public void findNullEntity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.findOne(null);
		});

		TestUtils.assertStoreException(thrown, "Error retrieving city with id null",
			"The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!",
			ErrorDomain.CITY, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while updating an inexistent city")
	public void updateInexistentEntity() throws StoreException {
		State state = MockUtils.createState("SP", "São Paulo");
		City entity = MockUtils.createCity("Itatiba", state);
		entity.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.update(entity, Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "city", Long.MAX_VALUE),
			ErrorDomain.CITY, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test if exception is thrown while deleting an inexistent city")
	public void deleteInexistentEntity() throws StoreException {
		State state = MockUtils.createState("SP", "São Paulo");
		City entity = MockUtils.createCity("Itatiba", state);
		entity.setId(Long.MAX_VALUE);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.delete(Long.MAX_VALUE);
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "city", Long.MAX_VALUE),
			ErrorDomain.CITY, ErrorCode.ENTITY_NOT_FOUND);
	}
	
	@Test
	@DisplayName("Test generic exception when saving city")
	public void persistNullCity() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.save(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save city",
			thrown.getDetails(), ErrorDomain.CITY, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test generic exception when saving city list")
	public void persistNullCityList() throws StoreException {
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.saveAll(null);
		});
		
		TestUtils.assertStoreException(thrown, "Could not save cities",
			thrown.getDetails(), ErrorDomain.CITY, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if a city without name is persisted")
	public void persistCityWithoutName() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City entity = MockUtils.createCity(null, state);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "City name"), ErrorDomain.CITY, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a city with empty name is persisted")
	public void persistCityWithEmptyName() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City entity = MockUtils.createCity("", state);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "City name"), ErrorDomain.CITY, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a city with name that exceeds limit is persisted")
	public void persistCityWithNameExceedingLimit() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City entity = MockUtils.createCity(RandomStringUtils.randomAlphabetic(300), state);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.STRING_GREATER, "City name", 255), ErrorDomain.CITY, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a city without state is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistCityWithoutState() throws StoreException {
		City entity = MockUtils.createCity("Itatiba", null);
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.save(entity);
		});
		
		TestUtils.assertStoreException(thrown, Constants.INVALID_VALUES,
			String.format(Constants.EMPTY_VALUE, "State"), ErrorDomain.CITY, ErrorCode.BAD_REQUEST);
	}
	
	@Test
	@DisplayName("Test if a city with inexistent state is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistCityWithInexistentState() throws StoreException {
		State state = MockUtils.createState("SP", "São Paulo");
		City entity = cityService.save(MockUtils.createCity("Itatiba", state));
		City entityFromDb = cityService.findOne(entity.getId());
		
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getState(), entity.getState());
		assertNotNull(entityFromDb.getState());
	}
	
	@Test
	@DisplayName("Test if a city is persisted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistCity() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City entity = cityService.save(MockUtils.createCity("Itatiba", state));

		City entityFromDb = cityService.findOne(entity.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getState(), entity.getState());
	}
	
	@Test
	@DisplayName("Test if an existing city is not persisted twice")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void persistExistingCity() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City entity = cityService.save(MockUtils.createCity("Itatiba", state));
		City entityFromDb = cityService.findOne(entity.getId());

		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getState(), entity.getState());
		
		entity = cityService.save(entity);
		entityFromDb = cityService.findOne(entity.getId());
		assertNotNull(entityFromDb);
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), entity.getName());
		assertEquals(entityFromDb.getState(), entity.getState());
	}
	
	@Test
	@DisplayName("Test if a list of cities is retrieved")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void findAll() throws StoreException {
		List<City> entitiesFromDb = cityService.findAll();
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		
		City itatiba = MockUtils.createCity("Itatiba", state);
		City campinas = MockUtils.createCity("Campinas", state);
		City saoPaulo = MockUtils.createCity("São Paulo", state);
		
		List<City> entities = new ArrayList<>();
		entities.add(itatiba);
		entities.add(campinas);
		entities.add(saoPaulo);
		entities = cityService.saveAll(entities);
		
		entitiesFromDb = cityService.findAll();
		assertEquals(entities.size(), entitiesFromDb.size());
		assertEquals(entities, entitiesFromDb);
	}
	
	@Test
	@DisplayName("Test generic exception when updating city")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void updateNullState() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		City city = cityService.save(MockUtils.createCity("Itatiba", state));
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.update(null, city.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not update city with id " + city.getId(),
			thrown.getDetails(), ErrorDomain.CITY, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if a city is updated")
	public void update() throws StoreException {
		State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		State stateToUpdate = stateService.save(MockUtils.createState("MG", "Minas Gerais"));
		City entity = cityService.save(MockUtils.createCity("Itatiba", state));
		
		assertNotNull(entity.getId());
		assertEquals(entity.getName(), "Itatiba");
		assertEquals(entity.getState(), state);
		
		entity.setName("Updated");
		entity.setState(stateToUpdate);
		cityService.update(entity, entity.getId());
		
		City entityFromDb = cityService.findOne(entity.getId());
		
		assertEquals(entityFromDb.getId(), entity.getId());
		assertEquals(entityFromDb.getName(), "Updated");
		assertEquals(entityFromDb.getState(), stateToUpdate);
	}
	
	@Test
	@DisplayName("Test if a city is not updated if it breaks unique constraint")
	public void updateFail() throws StoreException {
		final State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		final City targetEntity = cityService.save(MockUtils.createCity("Itatiba", state));
		final City entityToUpdate = cityService.save(MockUtils.createCity("Ita", state));
		
		assertNotNull(targetEntity.getId());
		assertNotNull(entityToUpdate.getId());
		
		entityToUpdate.setName("Itatiba");
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.update(entityToUpdate, entityToUpdate.getId());
		});
		
		TestUtils.assertStoreException(thrown, "Could not update city with id " + entityToUpdate.getId(),
			"City already exists", ErrorDomain.CITY, ErrorCode.SERVER_ERROR);
	}
	
	@Test
	@DisplayName("Test if a city can be deleted")
	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	public void deleteCity() throws StoreException {
		final State state = stateService.save(MockUtils.createState("SP", "São Paulo"));
		final City city = cityService.save(MockUtils.createCity("Itatiba", state));
		
		assertNotNull(city.getId());
		assertEquals(city.getName(), "Itatiba");
		assertEquals(city.getState(), state);
		
		cityService.delete(city.getId());
		
		StoreException thrown = assertThrows(StoreException.class, () -> {
			cityService.findOne(city.getId());
		});
		
		TestUtils.assertStoreException(thrown, Constants.ENTITY_NOT_FOUND,
			String.format(Constants.ENTITY_NOT_FOUND_MESSAGE, "city", city.getId()),
			ErrorDomain.CITY, ErrorCode.ENTITY_NOT_FOUND);
	}
	
}
