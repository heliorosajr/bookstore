# Livraria
A aplicação se refere à uma livraria, onde os livros podem ser adicionados ao carrinho de compras do cliente.

## Table of Contents
 * [Autenticação](#autenticacao)
 * [Entidades](#entidades)
 * [Controllers](#controllers)
 * [Testes](#testes)
 * [Banco de dados](#banco-de-dados)
 * [Melhorias](#melhorias)
 * [Knwon Issues](#known-issues)
 * [Contato](#contato)

## Autenticação

Para utilizar os endpoints da aplicação é necessário se autenticar através do endpoint /auth/siginin.

Após submeter o pedido de autenticação a aplicação irá retornar um token, o qual deve ser utilizado no header das futuras requisições. Este token tem duração de 1h.

## Entidades

As entidades principais do sistema são ```User.java```, ```Customer.java```, ```Employee.java```, ```Book.java``` e ```Cart.java```.

## Controllers

1. Authentication

Responsável por authenticar os usuários.

2. Employee

Responsável por buscar (único ou todos), criar, atualizar e excluir funcionários.

3. Customer

Responsável por buscar (único ou todos), criar, atualizar e excluir clientes.

4. Book

Responsável por buscar (único ou todos), criar, atualizar e excluir livros, e também atalizar a quantidade em estoque.
_*Implementação não foi concluída*_

5. Cart

Responsável por buscar (único ou todos), criar, atualizar e excluir carrinhos de compra, e também atalizar finalizar ou cancelar o carrinho.
_*Implementação não foi concluída*_

## Testes

Foram criados testes unitários utlizando h2 para as classes de serviço e também para os controllers.

De acordo com os resultados do jacoco, atualmente 60% do código está coberto pelos testes - _*lembrando que os módulos de livro e carrinho NÃO estão completos*_.

Para obter os resultados da cobertura, execute o comando abaixo, e os resultados estão disponíveis em ```<path>/bookstore/target/site/jacoco/index.html```
```shell
$ mvn clean install test
```

## Melhorias

- Aumento da cobertura de código nos testes
- Implementação dos módulos incompletos
- Integração com Jenkins
- Adicionar flyway para migração de dados
- Criação de scripts ansible

## Banco de dados

Um dos requisitos era a utilização do banco de dados Oracle. Peço desculpas, mas não consegui fazer a instalação na minha máquina (uso um Mac), e devido a isso eu utilizei MySQL.

Dado o escopo da aplicação, isso pode ser facilmente modificado apenas alterando os atributos de conexão definidos em ```application.properties```.  Mantive no diretório de resources o script para criação e população do banco MySQL com dados mockados.

## Known Issues

Infelizmente não consegui terminar a implementação dos módulos de livro e carrinho. Portanto essas funcionalidades não foram devidamente testadas e validadas.

### Contato
- [Hélio De Rosa Junior](mailto:helio_junior@apple.com) (11) 9 97195-5771